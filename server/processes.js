const request = require("request");
const admin = require("firebase-admin");
var fs = require("fs");
var formidable = require("formidable");
var Imgur = require("imgur");

const database = require("./database");
const email = require("./email");

Imgur.setClientId("f391d16340718af");
Imgur.setAPIUrl("https://api.imgur.com/3/");

exports.getProcesses = (req, res) => {
  var data = {
    userId: req.query.userId
  };

  console.log(data);
  getProcesses(data, (response) => {
    res.json(response);
  });
};

exports.getProcess = (req, res) => {
  var data = {
    process: req.params.process,
    userId: req.query.userId
  };

  console.log(data);
  getProcess(data, (response) => {
    res.json(response);
  });
};

exports.getSelectables = (req, res) => {
  var data = {
    process: req.params.process,
    userId: req.query.userId
  };

  console.log(data);
  getSelectables(data, (response) => {
    res.json(response);
  });
};

exports.createProcess = (req, res) => {
  var data = {
    process: req.body.process,
    userId: req.query.userId
  };

  console.log(data);
  createProcess(data, (response) => {
    res.json(response);
  });
};

exports.editProcess = (req, res) => {
  var data = {
    id: req.params.process,
    process: req.body.process,
    userId: req.query.userId
  };

  console.log(data);
  editProcess(data, (response) => {
    res.json(response);
  });
};

exports.uploadPhotos = (req, res) => {
  var form = new formidable.IncomingForm({
	  uploadDir: __dirname,
	  keepExtensions: true
	});

  form.parse(req, function (err, fields, files) {
		var path = files.fileToUpload.path;

    Imgur.uploadFile(path)
  	.then(function (json) {
  		res.json(json.data.link);

      fs.unlink(path, (err) => {
        if (err) console.error(err);
      });
  	})
  	.catch(function (err) {
    	res.json({error: err.message});

      fs.unlink(path, (err) => {
        if (err) console.error(err);
      });
  	});
  });
}

exports.deleteProcess = (req, res) => {
  var data = {
    process: req.params.process,
    userId: req.query.userId
  };

  console.log(data);
  deleteProcess(data, (response) => {
    res.json(response);
  });
};

exports.acceptProcess = (req, res) => {
  var data = {
    process: req.params.process,
    userId: req.query.userId
  };

  console.log(data);
  acceptProcess(data, (response) => {
    res.json(response);
  });
};

var populateFields = (doc, callback) => {
  let data = doc.data();
  let hasAnimal = false, hasType = false, hasResponsible = false;

  data.animal.get()
  .then(item => {
    data.animal = {id: item.id, info: item.data()};
    hasAnimal = true;

    if(hasAnimal && hasType && hasResponsible){
      callback({id: doc.id, info: data});
    }
  })
  .catch(err => console.error(err));

  data.type.get()
  .then(item => {
    data.type = {value: item.id, label: item.data().name};
    hasType = true;

    if(hasAnimal && hasType && hasResponsible){
      callback({id: doc.id, info: data});
    }
  })
  .catch(err => console.error(err));

  if(data.responsible){
    admin.auth().getUser(data.responsible)
    .then((user) => {
      if(user){
        data.responsible = {value: user.uid, label: user.displayName};
        hasResponsible = true;

        if(hasAnimal && hasType && hasResponsible){
          callback({id: doc.id, info: data});
        }
      }
    })
    .catch(err => console.error(err));
  }else{
    data.responsible = {value: "", label: ""};
    hasResponsible = true;

    if(hasAnimal && hasType && hasResponsible){
      callback({id: doc.id, info: data});
    }
  }
}

var getProcesses = (data, callback) => {
  admin.firestore().collection("processes").get()
  .then((snapshot) => {
    let res = [];
    let itemsPopulated = 0;

    snapshot.forEach((doc, i) => {
      populateFields(doc, (data) => {
        res.push(data);
        itemsPopulated++;

        if(itemsPopulated === snapshot.size){
          callback(res);
        }
      });
    });
  });
};

var getProcess = (data, callback) => {
  admin.firestore().collection("processes").doc(data.process).get()
  .then((doc) => {
    populateFields(doc, callback);
  });
};

var getSelectables = (data, callback) => {
  let res = [];
  let hasAnimals = false, hasTypes = false, hasResponsibles = false;

  admin.firestore().collection("animals").get()
  .then((snapshot) => {
    let animals = [];

    snapshot.forEach((doc, i) => {
      animals.push({id: doc.id, info: doc.data()});
    });

    res.push({ animals });
    hasAnimals = true;

    if(hasAnimals && hasTypes && hasResponsibles){
      callback(res);
    }
  });

  admin.firestore().collection("types").get()
  .then((snapshot) => {
    let types = [];

    snapshot.forEach((doc, i) => {
      types.push({value: doc.id, label: doc.data().name});
    });

    res.push({ types });
    hasTypes = true;

    if(hasAnimals && hasTypes && hasResponsibles){
      callback(res);
    }
  });

  admin.auth().listUsers()
  .then((snapshot) => {
    var responsibles = [];

    snapshot.users.forEach((user) => {
      let responsible = {value: user.toJSON().uid, label: user.toJSON().displayName};
      responsibles.push(responsible);
    });

    res.push({ responsibles });
    hasResponsibles = true;

    if(hasAnimals && hasTypes && hasResponsibles){
      callback(res);
    }
  });
};

var createProcess = (data, callback) => {
  data.process.animal =  admin.firestore().doc("/animals/" + data.process.animal.id);
  data.process.type =  admin.firestore().doc("/types/" + data.process.type.value);
  if(data.process.responsible.value !== ""){
    data.process.responsible = data.process.responsible.value;
  }else{
    delete data.process.responsible;
  }

  admin.firestore().collection("processes").add(data.process)
  .then(() => {
    getProcesses(data, (processes) => {
      callback(processes);
    });
  });
};

var editProcess = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      data.process.animal =  admin.firestore().doc("/animals/" + data.process.animal.id);
      data.process.type =  admin.firestore().doc("/types/" + data.process.type.value);
      data.process.responsible = data.process.responsible.value;

      admin.firestore().collection("processes").doc(data.id).update(data.process)
      .then(() => {
        getProcesses(data, (processes) => {
          callback(processes);
        });
      });
    }
  });
};

var deleteProcess = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      admin.firestore().collection("processes").doc(data.process).delete()
      .then(() => {
        getProcesses(data, (processes) => {
          callback(processes);
        });
      });
    }
  });
};

var acceptProcess = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      let responsible = data.userId;

      admin.firestore().collection("processes").doc(data.process).update({ accepted: true, responsible })
      .then(() => {
        getProcesses(data, (processes) => {
          callback(processes);
        });
      })
      .then(() => {
        getProcess(data, (response) => {
          email.sendEmail(
            response.info.email,
            "Sobreviver: Processo aceite",
            "<h2>O seu processo de " + response.info.type.label
            + (response.info.animal.info.gender.value == "male" ? " do " : " da ")
            + response.info.animal.info.name + " foi aceite</h2>"
            + "<img src=" + response.info.animal.info.images[0] + "/>"
          );
        });
      });
    }
  });
};
