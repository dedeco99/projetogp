const request = require("request");
const admin = require("firebase-admin");

const database = require("./database");

exports.getContent = (req, res) => {
  var data = {
    userId: req.query.userId
  };
  
  console.log(data);
  getContent(data, (response) => {
    res.json(response);
  });
};

exports.editContent = (req, res) => {
  var data = {
    id: req.params.content,
    content: req.body.content,
    userId: req.query.userId
  };

  console.log(data);
  editContent(data, (response) => {
    res.json(response);
  });
};

var getContent = (data, callback) => {
  admin.firestore().collection("content").get()
  .then((snapshot) => {
    let res = [];

    snapshot.forEach((doc, i) => {
      res.push({id: doc.id, info: doc.data()});
    });

    callback(res);
  });
};

var editContent = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      admin.firestore().collection("content").doc(data.id).update(data.content)
      .then(() => {
        getContent(data, (content) => {
          callback(content);
        });
      });
    }
  });
};
