const request = require("request");
const admin = require("firebase-admin");

const database = require("./database");
const email = require("./email");

exports.getEvents = (req, res) => {
  var data = {
    animal: req.params.animal,
    userId: req.query.userId
  };

  console.log(data);
  getEvents(data, (response) => {
    res.json(response);
  });
};

exports.getEvent = (req, res) => {
  var data = {
    event: req.params.event,
    userId: req.query.userId
  };

  console.log(data);
  getEvent(data, (response) => {
    res.json(response);
  });
};

exports.getSelectables = (req, res) => {
  var data = {
    process: req.params.process,
    userId: req.query.userId
  };

  console.log(data);
  getSelectables(data, (response) => {
    res.json(response);
  });
};

exports.createEvent = (req, res) => {
  var data = {
    event: req.body.event,
    userId: req.query.userId
  };

  console.log(data);
  createEvent(data, (response) => {
    res.json(response);
  });
};

exports.editEvent = (req, res) => {
  var data = {
    id: req.params.event,
    event: req.body.event,
    userId: req.query.userId
  };

  console.log(data);
  editEvent(data, (response) => {
    res.json(response);
  });
};

exports.deleteEvent = (req, res) => {
  var data = {
    event: req.params.event,
    userId: req.query.userId
  };

  console.log(data);
  deleteEvent(data, (response) => {
    res.json(response);
  });
};

var populateFields = (doc, callback) => {
  let data = doc.data();
  let hasAnimal = false, hasResponsible = false;

  data.animal.get()
  .then(item => {
    data.animal = {id: item.id, info: item.data()};
    hasAnimal = true;

    if(hasAnimal && hasResponsible){
      callback({id: doc.id, info: data});
    }
  })
  .catch(err => console.error(err));

  admin.auth().getUser(data.responsible)
  .then((user) => {
    if(user){
      data.responsible = user;
      hasResponsible = true;

      if(hasAnimal && hasResponsible){
        callback({id: doc.id, info: data});
      }
    }
  })
  .catch(err => console.error(err));
}

var getEvents = (data, callback) => {
  admin.firestore().collection("events").get()
  .then((snapshot) => {
    let res = [];
    let itemsPopulated = 0;

    snapshot.forEach((doc, i) => {
      populateFields(doc, (item) => {
        res.push(item);
        itemsPopulated++;

        if(itemsPopulated === snapshot.size){
          callback(res.filter((event) => {
            return event.info.animal.id === data.animal
          }));
        }
      });
    });
  });
};

var getEvent = (data, callback) => {
  admin.firestore().collection("events").doc(data.event).get()
  .then((doc) => {
    populateFields(doc, callback);
  });
};

var getSelectables = (data, callback) => {
  let res = [];
  let hasAnimals = false, hasResponsibles = false;

  admin.firestore().collection("animals").get()
  .then((snapshot) => {
    let animals = [];

    snapshot.forEach((doc, i) => {
      animals.push({id: doc.id, info: doc.data()});
    });

    res.push({ animals });
    hasAnimals = true;

    if(hasAnimals && hasResponsibles){
      callback(res);
    }
  });

  admin.auth().listUsers()
  .then((snapshot) => {
    var responsibles = [];

    snapshot.users.forEach((user) => {
      responsibles.push(user.toJSON());
    });

    res.push({ responsibles });
    hasResponsibles = true;

    if(hasAnimals && hasResponsibles){
      callback(res);
    }
  });
};

var createEvent = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      data.event.animal =  admin.firestore().doc("/animals/" + data.event.animal.id);
      data.event.responsible = data.event.responsible.uid;

      admin.firestore().collection("events").add(data.event)
      .then(() => {
        getEvents(data, (events) => {
          callback(events);
        });
      });
    }
  });
};

var editEvent = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      data.event.animal =  admin.firestore().doc("/animals/" + data.event.animal.id);
      data.event.responsible = data.event.responsible.uid;

      admin.firestore().collection("events").doc(data.id).update(data.event)
      .then(() => {
        getEvents(data, (events) => {
          callback(events);
        });
      });
    }
  });
};

var deleteEvent = (data, callback) => {
  getEvent(data, (event) => {
    data.animal = event.info.animal.id;

    admin.auth().getUser(data.userId)
    .then((user) => {
      if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
        admin.firestore().collection("events").doc(data.event).delete()
        .then(() => {
          getEvents(data, (events) => {
            callback(events);
          });
        });
      }
    });
  });
};
