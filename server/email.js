const nodemailer = require("nodemailer");

exports.sendEmail = (email, subject, message, callback) => {
	var transporter = nodemailer.createTransport({
		service: "gmail",
		auth: {
			user: "alberguesobreviver@gmail.com",
			pass: "sobreviver-00"
		}
	});

	const mailOptions = {
		from: "alberguesobreviver@gmail.com",
		to: email,
		subject: subject,
		html: message
	};

	transporter.sendMail(mailOptions, (err, info) => {
	   if(err){
			 callback({ message: "Erro a enviar email", color: "red-text" });
		 }else{
			 callback({ message: "Email enviado com sucesso! Verifique o seu email para renovar a sua password", color: "green-text" });
		 }
	});
}
