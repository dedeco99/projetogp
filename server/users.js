const request = require("request");
const admin = require("firebase-admin");

const database = require("./database");
const email = require("./email");

exports.forgotPassword = (req, res) => {
  var data = {
    email: req.params.email
  };

  console.log(data);
  forgotPassword(data, (response) => {
    res.json(response);
  });
};

exports.getUsers = (req, res) => {
  var data = {
    userId: req.query.userId
  };

  console.log(data);
  getUsers(data, (response) => {
    res.json(response);
  });
};

exports.getUser = (req, res) => {
  var data = {
    user: req.params.user,
    userId: req.query.userId
  };

  console.log(data);
  getUser(data, (response) => {
    res.json(response);
  });
};

exports.createUser = (req, res) => {
  var data = {
    user: req.body,
    userId: req.query.userId
  };

  console.log(data);
  createUser(data, (response) => {
    res.json(response);
  });
};

exports.editUser = (req, res) => {
  var data = {
    id: req.params.user,
    user: req.body,
    userId: req.query.userId
  };

  console.log(data);
  editUser(data, (response) => {
    res.json(response);
  });
};

exports.deleteUser = (req, res) => {
  var data = {
    user: req.params.user,
    userId: req.query.userId
  };

  console.log(data);
  deleteUser(data, (response) => {
    res.json(response);
  });
};

exports.setAsAdmin = (req, res) => {
  var data = {
    user: req.params.user,
    userId: req.query.userId
  };

  console.log(data);
  setAsAdmin(data, (response) => {
    res.json(response);
  });
};

exports.getLogs = (req, res) => {
  var data = {
    userId: req.query.userId
  };

  console.log(data);
  getLogs(data, (response) => {
    res.json(response);
  });
};

var forgotPassword = (data, callback) => {
  const userEmail = data.email;

  admin.auth().generatePasswordResetLink(userEmail)
  .then((link) => {
    email.sendEmail(
      userEmail,
      "Sobreviver: Renovar password",
      "Clique no link para renovar password: " + link,
      callback
    );
  }).catch((error) => {
    callback({ message: "O email inserido não existe", color: "red-text" });
  });
}

var getUsers = (data, callback) => {
  admin.auth().listUsers()
  .then((snapshot) => {
    if(snapshot.users.length > 0) {
      var res = [];

      for(var i = 0; i < snapshot.users.length; i++){
        res.push(snapshot.users[i].toJSON());
      }

      callback(res);
    }else{
      callback("no records")
    }
  });
};

var getUser = (data, callback) => {
  admin.auth().getUser(data.user)
  .then((user) => {
    if(user){
      callback(user);
    }
  });
};

var createUser = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      admin.auth().createUser({
        email: data.user.user.email,
        password: data.user.user.password,
        displayName: data.user.user.name
      })
      .then(() => {
        getUsers(data, (users) => {
          callback(users);
        });
      })
      .catch((error) => {
        callback({ error: error })
      });
    }
  });
};

var editUser = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if((data.id === data.userId) || (user.toJSON().customClaims && user.toJSON().customClaims.admin)){
      admin.auth().updateUser(data.id, {
        email: data.user.user.email,
        password: data.user.user.password,
        displayName: data.user.user.name
      })
      .then(() => {
        getUsers(data, (users) => {
          callback(users);
        });
      })
      .catch((error) => {
        callback({ error: error })
      });
    }
  });
};

var deleteUser = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      admin.auth().deleteUser(data.user)
      .then(() => {
        getUsers(data, (users) => {
          callback(users);
        });
      });
    }
  });
};

var setAsAdmin = (data, callback) => {
  var userId = data.user;
  var adminClaim = true;

  if(userId !== data.userId){
    admin.auth().getUser(data.userId)
    .then((adminUser) => {
      if(adminUser.toJSON().customClaims && adminUser.toJSON().customClaims.admin){
        admin.auth().getUser(userId)
        .then((user) => {
          if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
            adminClaim = false;
          }

          admin.auth().setCustomUserClaims(userId, {admin: adminClaim})
          .then(() => {
            getUsers(data, (users) => {
              callback(users);
            });
          });
        });
      }
    });
  }
};

var getLogs = (data, callback) => {
  admin.firestore().collection("logs").get()
  .then((snapshot) => {
    let res = [];

    snapshot.forEach((doc, i) => {
      res.push(doc.data());
    });

    callback(res);
  });
};
