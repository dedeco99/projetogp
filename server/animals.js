const request = require("request");
const admin = require("firebase-admin");
const formidable = require("formidable");
const fs = require("fs");
const Imgur = require("imgur");
const Twitter = require("twitter");
const Client = require("instagram-private-api").V1;
var sharp = require("sharp");
const whatDog=require("what-dog");

const database = require("./database");
const email = require("./email");

Imgur.setClientId("f391d16340718af");
Imgur.setAPIUrl("https://api.imgur.com/3/");

var client = new Twitter({
  consumer_key: "KPR6w5YqwTzCR1ASW0fiovqa2",
  consumer_secret: "Jv4xQ4e7AZhO6e0WlB7qBi6S2d01mTDSrc4YnoDIaRnQHnlNtO",
  access_token_key: "1113805625434214401-x8kNwmQadWjjgurZfR3YbcDeisIObl",
  access_token_secret: "Yvd5KSDOTRTwBKmYhgQ9x5vAaKGqyGOHm9tgSvUVs7Z5f"
});

exports.getAnimals = (req, res) => {
  var data = {
    userId: req.query.userId
  };

  console.log(data);
  getAnimals(data, (response) => {
    res.json(response);
  });
};

exports.getAnimal = (req, res) => {
  var data = {
    animal: req.params.animal,
    userId: req.query.userId
  };

  console.log(data);
  getAnimal(data, (response) => {
    res.json(response);
  });
};

exports.createAnimal = (req, res) => {
  var data = {
    animal: req.body.animal,
    userId: req.query.userId
  };

  console.log(data);
  createAnimal(data, (response) => {
    res.json(response);
  });
};

exports.editAnimal = (req, res) => {
  var data = {
    id: req.params.animal,
    animal: req.body.animal,
    userId: req.query.userId
  };

  console.log(data);
  editAnimal(data, (response) => {
    res.json(response);
  });
};

exports.uploadPhotos = (req, res) => {
  var data = {
    type: req.params.type,
    postOnTwitter: req.query.postOnTwitter,
    postOnInstagram: req.query.postOnInstagram,
  };

  var animal = {
    name: req.query.name,
    type: req.query.type,
    gender: req.query.gender
  };

  var form = new formidable.IncomingForm({
	  uploadDir: __dirname,
	  keepExtensions: true
	});

  form.parse(req, function (err, fields, files) {
		var path = files.fileToUpload.path;

    if(data.postOnTwitter === 'true'){
      twitter(animal, path);
    }

    if(data.postOnInstagram === 'true'){
      var newPath = path.substring(0, path.lastIndexOf(".")) + "_min" + path.substring(path.lastIndexOf("."));
      console.log(newPath);
      sharp(path)
		  .toFile(newPath)
		  .then(data => {
        instagram(animal, newPath);
      });
    }

    Imgur.uploadFile(path)
  	.then(function (json) {
      if(data.type === "dog"){
        whatDog(json.data.link).then(data => {
          res.json({ breed: data.breed, link: json.data.link });
        });
      }else{
        res.json({ link: json.data.link });
      }

      fs.unlink(path, () => {
        console.log("File deleted from server");
      });
  	})
  	.catch(function (err) {
    	res.json({error: err.message});
  	});
  });
}

exports.deleteAnimal = (req, res) => {
  var data = {
    animal: req.params.animal,
    userId: req.query.userId
  };

  console.log(data);
  deleteAnimal(data, (response) => {
    res.json(response);
  });
};

var getAnimals = (data, callback) => {
  admin.firestore().collection("animals").get()
  .then((snapshot) => {
    let res = [];

    snapshot.forEach((doc, i) => {
      res.push({id: doc.id, info: doc.data()});
    });

    callback(res);
  });
};

var getAnimal = (data, callback) => {
  admin.firestore().collection("animals").doc(data.animal).get()
  .then((doc) => {
    callback({id: doc.id, info: doc.data()});
  });
};

var createAnimal = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      admin.firestore().collection("animals").add(data.animal)
      .then(() => {
        getAnimals(data, (animals) => {
          callback(animals);
        });
      });
    }
  });
};

var editAnimal = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      admin.firestore().collection("animals").doc(data.id).update(data.animal)
      .then(() => {
        getAnimals(data, (animals) => {
          callback(animals);
        });
      });
    }
  });
};

var deleteAnimal = (data, callback) => {
  admin.auth().getUser(data.userId)
  .then((user) => {
    if(user.toJSON().customClaims && user.toJSON().customClaims.admin){
      admin.firestore().collection("animals").doc(data.animal).delete()
      .then(() => {
        getAnimals(data, (animals) => {
          callback(animals);
        });
      });
    }
  });
};

var twitter = (animal, path) => {
  var data = fs.readFileSync(path);

	client.post("media/upload", { media: data }, function(error, media, response) {
    if (!error) {
      var status = {
	      status: animal.name + ", " + animal.type + " " + animal.gender,
	      media_ids: media.media_id_string
	    }

	    client.post("statuses/update", status, function(error, tweet, response) {
	      if (error) console.log(error);
	    });
	  }else{
      console.log(error);
    }
	});
}

var instagram = (animal, path) => {
	var device = new Client.Device("alberguesobreviver");
	var storage = new Client.CookieFileStorage(__dirname + "/cookies/someuser.json");

	Client.Session.create(device, storage, "alberguesobreviver", "dedeco-00")
	.then(function(session) {
		/*var medias = [
			{
        type: "photo",
        size: [1080, 1080],
        data: "./public/images/img.jpeg"
	    },
			{
        type: "photo",
        size: [1080, 1080],
        data: "." + path
	    }
		];

		Client.Upload.album(session, medias)
    .then(function (payload) {
        Client.Media.configureAlbum(session, payload, "akward caption")
    })
    .then(function () {
        // we configure album
    });*/
		Client.Upload.photo(session, path)
		.then(function(upload) {
			console.log(upload.params.uploadId);
			Client.Media.configurePhoto(session, upload.params.uploadId, animal.name + ", " + animal.type + " " + animal.gender)
			.then(function(res){
				console.log(res.params.code);
			});
		})
	});
}
