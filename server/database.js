const admin = require("firebase-admin");

exports.initialize = () => {
	admin.initializeApp({
		credential: admin.credential.cert({
			project_id: process.env.FIREBASE_PROJECT_ID,
			private_key: process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
			client_email: process.env.FIREBASE_CLIENT_EMAIL,
		})
	});

	exports.firestore = admin.firestore();
}

exports.firestore = null;
