export const getEvents = (animal, userId) => {
	return (dispatch, getState) => {
		fetch("/api/animals/" + animal + "/events?userId=" + userId)
		.then(res => res.json())
		.then(events => {
			dispatch({ type: "GET_EVENTS", events })
		}).catch(error => {
			console.log("GET_EVENTS_ERROR", error.message);
		});
	}
};

export const getEvent = (id, userId, callback) => {
	return (dispatch, getState) => {
		fetch("/api/events/" + id + "?userId=" + userId)
		.then(res => res.json())
		.then(res => {
			callback(res)
		})
		.catch(error => {
			console.log("GET_EVENT_ERROR", error.message);
		});
	}
};

export const getSelectables = (id, userId, callback) => {
	return (dispatch, getState) => {
		fetch("/api/events/" + id + "/selectables?userId=" + userId)
		.then(res => res.json())
		.then(selectables => {
			callback(selectables);
		})
		.catch(error => {
			console.log("GET_SELECTABLES_ERROR", error.message);
		});
	}
};

export const createEvent = (event, userId) => {
	delete event.files;
	delete event.selectables;

	return (dispatch, getState) => {
		fetch("/api/events?userId=" + userId, { method: "POST", body: JSON.stringify({ event }),
    headers: {"Content-Type": "application/json"} })
		.then(res => res.json())
		.then(events => {
			dispatch({ type: "CREATE_EVENT", events })
			dispatch({ type: "SHOW_TOAST", message: "Evento adicionado com sucesso" })
		}).catch(error => {
			console.log("CREATE_EVENT_ERROR", error.message);
		});
	}
};

export const editEvent = (id, event, userId) => {
	delete event.files;
	delete event.selectables;

	return (dispatch, getState) => {
		fetch("/api/events/" + id + "?userId=" + userId, { method: "PUT", body: JSON.stringify({ event }),
    headers: {"Content-Type": "application/json"} })
		.then(res => res.json())
		.then(events => {
			dispatch({ type: "EDIT_EVENT", events })
			dispatch({ type: "SHOW_TOAST", message: "Evento atualizado com sucesso" })
		}).catch(error => {
			console.log("EDIT_EVENT_ERROR", error.message);
		});
	}
};

export const deleteEvent = (event, userId) => {
	return (dispatch, getState) => {
		fetch("/api/events/" + event + "?userId=" + userId, { method: "DELETE" })
		.then(res => res.json())
		.then(events => {
			dispatch({ type: "DELETE_EVENT", events })
			dispatch({ type: "SHOW_TOAST", message: "Evento removido com sucesso" })
		}).catch(error => {
			console.log("DELETE_EVENT_ERROR", error.message);
		});
	}
};

export const resetRedirect = () => {
	return (dispatch, getState) => {
		dispatch({ type: "RESET_REDIRECT" });
	}
};
