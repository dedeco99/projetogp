export const getContent = (userId) => {
	return (dispatch, getState) => {
		fetch("/api/content?userId=" + userId)
		.then(res => res.json())
		.then(content => {
			dispatch({ type: "GET_CONTENT", content })
		}).catch(error => {
			console.log("GET_CONTENT_ERROR", error.message);
		});
	}
};

export const editContent = (id, content, userId) => {
	return (dispatch, getState) => {
		fetch("/api/content/" + id + "?userId=" + userId, { method: "PUT", body: JSON.stringify({ content }),
    headers: {"Content-Type": "application/json"} })
		.then(res => res.json())
		.then(content => {
			dispatch({ type: "EDIT_CONTENT", content });
			dispatch({ type: "SHOW_TOAST", message: "Conteúdo atualizado com sucesso" })
		}).catch(error => {
			console.log("EDIT_CONTENT_ERROR", error.message);
		});
	}
};

export const resetRedirect = () => {
	return (dispatch, getState) => {
		dispatch({ type: "RESET_REDIRECT" });
	}
};
