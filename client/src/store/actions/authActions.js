export const login = (credentials) => {
	return (dispatch, getState, { getFirebase }) => {
		const firebase = getFirebase();

		firebase.auth().signInWithEmailAndPassword(
			credentials.email,
			credentials.password
		).then((user) => {
			dispatch({ type: "LOGIN_OK" })
		}).catch((error) => {
			dispatch({ type: "SHOW_TOAST", message: getErrorMessage(error.code), toastType: "error" })
		});
	}
}

export const forgotPassword = (email) => {
	return (dispatch, getState) => {
		fetch("/api/users/" + email + "/forgotPassword")
		.then((res) => res.json())
		.then((message) => {
			dispatch({ type: "FORGOT_PASSWORD_OK", message })
		}).catch((error) => {
			console.log("FORGOT_PASSWORD_ERROR", error.message);
		});
	}
}

export const logout = () => {
	return (dispatch, getState, { getFirebase }) =>{
		const firebase = getFirebase();

		firebase.auth().signOut()
		.then(() => {
			dispatch({ type: "LOGOUT_OK" })
		});
	}
}

const getErrorMessage = (code) => {
	switch(code){
		case "auth/user-not-found":
			return "Este utilizador não existe";
		case "auth/wrong-password":
			return "A password inserida não é válida";
		default:
			return "Credenciais inválidas";
	}
}
