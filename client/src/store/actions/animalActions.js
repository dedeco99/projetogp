export const getAnimals = (userId, callback) => {
	return (dispatch, getState) => {
		fetch("/api/animals?userId=" + userId)
		.then(res => res.json())
		.then(animals => {
			dispatch({ type: "GET_ANIMALS", animals })
			callback(animals);
		}).catch(error => {
			console.log("GET_ANIMALS_ERROR", error.message);
		});
	}
};

export const getAnimal = (id, userId, callback) => {
	return (dispatch, getState) => {
		fetch("/api/animals/" + id + "?userId=" + userId)
		.then(res => res.json())
		.then(animal => {
			callback(animal)
		})
		.catch(error => {
			console.log("GET_ANIMAL_ERROR", error.message);
		});
	}
};

export const createAnimal = (animal, userId) => {
	delete animal.genders;
	delete animal.types;
	delete animal.files;
	delete animal.identifiedRace;
	delete animal.frontError;
	delete animal.openDialog;

	return (dispatch, getState) => {
		fetch("/api/animals?userId=" + userId, { method: "POST", body: JSON.stringify({ animal }),
    headers: {"Content-Type": "application/json"} })
		.then(res => res.json())
		.then(animals => {
			dispatch({ type: "CREATE_ANIMAL", animals })
			dispatch({ type: "SHOW_TOAST", message: "Animal adicionado com sucesso" })
		}).catch(error => {
			console.log("CREATE_ANIMAL_ERROR", error.message);
		});
	}
};

export const editAnimal = (id, animal, userId) => {
	delete animal.genders;
	delete animal.types;
	delete animal.files;
	delete animal.identifiedRace;
	delete animal.frontError;
	delete animal.openDialog;

	return (dispatch, getState) => {
		fetch("/api/animals/" + id + "?userId=" + userId, { method: "PUT", body: JSON.stringify({ animal }),
    headers: {"Content-Type": "application/json"} })
		.then(res => res.json())
		.then(animals => {
			dispatch({ type: "EDIT_ANIMAL", animals })
			dispatch({ type: "SHOW_TOAST", message: "Animal atualizado com sucesso" })
		}).catch(error => {
			console.log("EDIT_ANIMAL_ERROR", error.message);
		});
	}
};

export const deleteAnimal = (animal, userId) => {
	return (dispatch, getState) => {
		fetch("/api/animals/" + animal + "?userId=" + userId, { method: "DELETE" })
		.then(res => res.json())
		.then(animals => {
			dispatch({ type: "DELETE_ANIMAL", animals })
			dispatch({ type: "SHOW_TOAST", message: "Animal removido com sucesso" })
		}).catch(error => {
			console.log("DELETE_ANIMAL_ERROR", error.message);
		});
	}
};

export const resetRedirect = () => {
	return (dispatch, getState) => {
		dispatch({ type: "RESET_REDIRECT" });
	}
};
