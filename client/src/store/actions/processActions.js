export const getProcesses = (userId, callback) => {
	return (dispatch, getState) => {
		fetch("/api/processes?userId=" + userId)
		.then(res => res.json())
		.then(processes => {
			dispatch({ type: "GET_PROCESSES", processes })
			callback(processes);
		}).catch(error => {
			console.log("GET_PROCESSES_ERROR", error.message);
		});
	}
};

export const getProcess = (id, userId, callback) => {
	return (dispatch, getState) => {
		fetch("/api/processes/" + id + "?userId=" + userId)
		.then(res => res.json())
		.then(process => {
			callback(process);
		})
		.catch(error => {
			console.log("GET_PROCESS_ERROR", error.message);
		});
	}
};

export const getSelectables = (id, userId, callback) => {
	return (dispatch, getState) => {
		fetch("/api/processes/" + id + "/selectables?userId=" + userId)
		.then(res => res.json())
		.then(selectables => {
			callback(selectables);
		})
		.catch(error => {
			console.log("GET_SELECTABLES_ERROR", error.message);
		});
	}
};

export const createProcess = (process, userId) => {
	delete process.files;
	delete process.selectables;
	delete process.posts;
	delete process.frontError;

	return (dispatch, getState) => {
		fetch("/api/processes?userId=" + userId, { method: "POST", body: JSON.stringify({ process }),
    headers: {"Content-Type": "application/json"} })
		.then(res => res.json())
		.then(processes => {
			dispatch({ type: "CREATE_PROCESS", processes })
			dispatch({ type: "SHOW_TOAST", message: "Processo adicionado com sucesso" })
		}).catch(error => {
			console.log("CREATE_PROCESS_ERROR", error.message);
		});
	}
};

export const editProcess = (id, process, userId) => {
	delete process.files;
	delete process.selectables;
	delete process.frontError;

	return (dispatch, getState) => {
		fetch("/api/processes/" + id + "?userId=" + userId, { method: "PUT", body: JSON.stringify({ process }),
    headers: {"Content-Type": "application/json"} })
		.then(res => res.json())
		.then(processes => {
			dispatch({ type: "EDIT_PROCESS", processes })
			dispatch({ type: "SHOW_TOAST", message: "Processo atualizado com sucesso" })
		}).catch(error => {
			console.log("EDIT_PROCESS_ERROR", error.message);
		});
	}
};

export const deleteProcess = (process, userId) => {
	return (dispatch, getState) => {
		fetch("/api/processes/" + process + "?userId=" + userId, { method: "DELETE" })
		.then(res => res.json())
		.then(processes => {
			dispatch({ type: "DELETE_PROCESS", processes })
			dispatch({ type: "SHOW_TOAST", message: "Processo removido com sucesso" })
		}).catch(error => {
			console.log("DELETE_PROCESS_ERROR", error.message);
		});
	}
};

export const acceptProcess = (process, userId) => {
	return (dispatch, getState) => {
		fetch("/api/processes/" + process + "?userId=" + userId, { method: "PATCH" })
		.then(res => res.json())
		.then(processes => {
			dispatch({ type: "ACCEPT_PROCESS", processes })
			dispatch({ type: "SHOW_TOAST", message: "Processo aceite com sucesso" })
		}).catch(error => {
			console.log("ACCEPT_PROCESS_ERROR", error.message);
		});
	}
};

export const resetRedirect = () => {
	return (dispatch, getState) => {
		dispatch({ type: "RESET_REDIRECT" });
	}
};
