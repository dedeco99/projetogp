export const getUsers = (userId) => {
	return (dispatch, getState) => {
		fetch("/api/users?userId=" + userId)
		.then(res => res.json())
		.then(users => {
			dispatch({ type: "GET_USERS", users })
		}).catch(error => {
			console.log("GET_USERS_ERROR", error.message);
		});
	}
};

export const createUser = (user, userId) => {
	return (dispatch, getState) => {
		fetch("/api/users?userId=" + userId, { method: "POST", body: JSON.stringify({ user }),
    headers: {"Content-Type": "application/json"} })
		.then(res => res.json())
		.then(users => {
			if(users.error){
				console.log(users.error)
				dispatch({ type: "SHOW_TOAST", message: getErrorMessage(users.error.code), toastType: "error" })
			}else{
				dispatch({ type: "CREATE_USER", users });
				dispatch({ type: "SHOW_TOAST", message: "Utilizador adicionado com sucesso" });
			}
		}).catch(error => {
			console.log("CREATE_USER_ERROR", error.message);
		});
	}
};

export const editUser = (id, user, userId) => {
	return (dispatch, getState) => {
		fetch("/api/users/" + id + "?userId=" + userId, { method: "PUT", body: JSON.stringify({ user }),
    headers: {"Content-Type": "application/json"} })
		.then(res => res.json())
		.then(users => {
			if(users.error){
				dispatch({ type: "SHOW_TOAST", message: getErrorMessage(users.error.code), toastType: "error" })
			}else{
				dispatch({ type: "EDIT_USER", users });
				dispatch({ type: "SHOW_TOAST", message: "Utilizador atualizado com sucesso" })
			}
		}).catch(error => {
			console.log("EDIT_USER_ERROR", error.message);
		});
	}
};

export const deleteUser = (user, userId) => {
	return (dispatch, getState) => {
		fetch("/api/users/" + user + "?userId=" + userId, { method: "DELETE" })
		.then(res => res.json())
		.then(users => {
			dispatch({ type: "DELETE_USER", users })
			dispatch({ type: "SHOW_TOAST", message: "Utilizador removido com sucesso" })
		}).catch(error => {
			console.log("DELETE_USER_ERROR", error.message);
		});
	}
};

export const setAsAdmin = (user, userId) => {
	return (dispatch, getState) => {
		fetch("/api/users/" + user + "/admin?userId=" + userId)
		.then(res => res.json())
		.then(users => {
			dispatch({ type: "ADMIN_USER", users })
		}).catch(error => {
			console.log("ADMIN_USER_ERROR", error.message);
		});
	}
};

export const resetRedirect = () => {
	return (dispatch, getState) => {
		dispatch({ type: "RESET_REDIRECT" });
	}
};

export const closeToast = () => {
	return (dispatch, getState) => {
		dispatch({ type: "CLOSE_TOAST" });
	}
};

const getErrorMessage = (code) => {
	switch(code){
		case "auth/invalid-password":
			return "A password têm de ter pelo menos 6 caratéres";
		case "auth/invalid-email":
			return "O email inserido não é válido";
		default:
			return "Credenciais inválidas";
	}
}

export const getLogs = (userId, callback) => {
	return (dispatch, getState) => {
		fetch("/api/logs?userId=" + userId)
		.then(res => res.json())
		.then(logs => {
			callback(logs)
		})
		.catch(error => {
			console.log("GET_LOGS_ERROR", error.message);
		});
	}
};
