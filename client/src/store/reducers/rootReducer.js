import { combineReducers } from "redux";
import { firestoreReducer } from "redux-firestore";
import { firebaseReducer } from "react-redux-firebase"

import authReducer from "./authReducer";
import userReducer from "./userReducer";
import animalReducer from "./animalReducer";
import processReducer from "./processReducer";
import eventReducer from "./eventReducer";
import contentReducer from "./contentReducer";

const rootReducer = combineReducers({
	auth: authReducer,
	user: userReducer,
	animal: animalReducer,
	process: processReducer,
	event: eventReducer,
	content: contentReducer,
	firestore: firestoreReducer,
	firebase: firebaseReducer
});

export default rootReducer;
