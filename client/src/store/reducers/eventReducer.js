const initState = {
	events: [],
	redirect: false
}

const eventReducer = (state = initState, action) => {
	switch(action.type){
		case "GET_EVENTS":
			console.log("Get events");
			return { ...state, events: action.events };
		case "CREATE_EVENT":
			console.log("Create event");
			return { ...state, events: action.events, redirect: true };
		case "EDIT_EVENT":
			console.log("Edit event");
			return { ...state, events: action.events, redirect: true };
		case "DELETE_EVENT":
			console.log("Delete event");
			return { ...state, events: action.events };
		case "RESET_REDIRECT":
			return { ...state, redirect: false };
		default:
			return state;
	}
}

export default eventReducer;
