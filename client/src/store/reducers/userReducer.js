const initState = {
	users: [],
	redirect: false,
	showToast: false,
	toastMessage: "",
	toastType: "",
}

const userReducer = (state = initState, action) => {
	switch(action.type){
		case "GET_USERS":
			console.log("Get users");
			return { ...state, users: action.users };
		case "CREATE_USER":
			console.log("Create user");
			return { ...state, users: action.users, redirect: true };
		case "EDIT_USER":
			console.log("Edit user");
			return { ...state, users: action.users, redirect: true };
		case "DELETE_USER":
			console.log("Delete user");
			return { ...state, users: action.users };
		case "ADMIN_USER":
			console.log("Admin user");
			return { ...state, users: action.users };
		case "RESET_REDIRECT":
			return { ...state, redirect: false };
		case "SHOW_TOAST":
		console.log(action)
			return { ...state, showToast: true, toastMessage: action.message, toastType: action.toastType };
		case "CLOSE_TOAST":
			return { ...state, showToast: false };
		default:
			return state;
	}
}

export default userReducer;
