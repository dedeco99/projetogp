const initState = {
	animals: [],
	redirect: false
}

const animalReducer = (state = initState, action) => {
	switch(action.type){
		case "GET_ANIMALS":
			console.log("Get animals");
			return { ...state, animals: action.animals };
		case "CREATE_ANIMAL":
			console.log("Create animal");
			return { ...state, animals: action.animals, redirect: true };
		case "EDIT_ANIMAL":
			console.log("Edit animal");
			return { ...state, animals: action.animals, redirect: true };
		case "DELETE_ANIMAL":
			console.log("Delete animal");
			return { ...state, animals: action.animals };
		case "RESET_REDIRECT":
			return { ...state, redirect: false };
		default:
			return state;
	}
}

export default animalReducer;
