const initState = {
	content: [],
	redirect: false
}

const contentReducer = (state = initState, action) => {
	switch(action.type){
		case "GET_CONTENT":
			console.log("Get content");
			return { ...state, content: action.content };
		case "EDIT_CONTENT":
			console.log("Edit content");
			return { ...state, content: action.content, redirect: true };
		case "RESET_REDIRECT":
			return { ...state, redirect: false };
		default:
			return state;
	}
}

export default contentReducer;
