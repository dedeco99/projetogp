const initState = {
}

const authReducer = (state = initState, action) => {
	switch(action.type){
		case "LOGIN_OK":
			console.log("Login successful");
			return { ...state, authError: null };
		case "FORGOT_PASSWORD_OK":
			console.log("Forgot password successful");
			return { ...state, forgotPasswordMessage: action.message };
		case "LOGOUT_OK":
			console.log("Logout successful");
			return { ...state, authError: null };
		default:
			return state;
	}
}

export default authReducer;
