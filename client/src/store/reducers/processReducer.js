const initState = {
	processes: [],
	redirect: false
}

const processReducer = (state = initState, action) => {
	switch(action.type){
		case "GET_PROCESSES":
			console.log("Get processes");
			return { ...state, processes: action.processes };
		case "CREATE_PROCESS":
			console.log("Create process");
			return { ...state, processes: action.processes, redirect: true };
		case "EDIT_PROCESS":
			console.log("Edit process");
			return { ...state, processes: action.processes, redirect: true };
		case "DELETE_PROCESS":
			console.log("Delete process");
			return { ...state, processes: action.processes };
		case "ACCEPT_PROCESS":
			console.log("Accept process");
			return { ...state, processes: action.processes };
		case "RESET_REDIRECT":
			return { ...state, redirect: false };
		default:
			return state;
	}
}

export default processReducer;
