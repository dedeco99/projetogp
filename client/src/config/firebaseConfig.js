import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

var config = {
  apiKey: "AIzaSyAi0OScGDcKJ8rc6J9RLNg1wxSNgFZxBG4",
  authDomain: "sobreviver-272f3.firebaseapp.com",
  databaseURL: "https://sobreviver-272f3.firebaseio.com",
  projectId: "sobreviver-272f3",
  storageBucket: "sobreviver-272f3.appspot.com",
  messagingSenderId: "509287573746"
};

firebase.initializeApp(config);
firebase.firestore();

export default firebase;
