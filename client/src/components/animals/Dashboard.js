import React, { Component } from "react";
import { connect } from "react-redux";

import { getAnimals } from "../../store/actions/animalActions";
import { getLogs } from "../../store/actions/userActions";

import cat from "../../img/cat.png";
import dog from "../../img/dog.png";
//import instagram from "../../img/instagram.png";
//import facebook from "../../img/facebook.png";

import "../../css/Dashboard.css";

class Dashboard extends Component {
  constructor(props){
    super(props);

    this.state = {
      groups: {
        type: [],
        gender: [],
        race:[]
      },
      logs: []
    }
  }

  calculateGroups(animals) {
    let { groups } = this.state;
    const groupables = ["type","gender", "race"];

    animals.forEach(animal => {
      groupables.forEach(groupable => {
        let groupExists = false;

        if(groups[groupable].length > 0){
          groups[groupable].forEach(group => {
            let name = animal.info[groupable].label;
            if(name === "") name = "Sem raça";

            if(group[0] === name){
              group[1]++;
              groupExists = true;
              return;
            }
          });
        }

        if(groups[groupable].length === 0 || !groupExists){
          let name = animal.info[groupable].label;
          if(name === "") name = "Sem raça";

          groups[groupable].push([name, 1 ]);
        }
      });
    });

    this.setState({ groups });
    return groups;
  }

  calculateMonths(animals) {
    const currentDate = new Date();
    const mesesTranslate = [
      "Mês", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
      "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
    ];
    let todayMonth = currentDate.getMonth() + 1;
    let todayYear = currentDate.getFullYear();
    let meses = [];

    for (let i = 0; i < 6; i++) {
        meses.push([mesesTranslate[todayMonth], 0, 0]);
        todayMonth--;
        if (todayMonth === 0) todayMonth = 12;
    }

    animals.forEach(animal => {
      todayMonth = currentDate.getMonth() + 1;
      todayYear = currentDate.getFullYear();

      for(let i = 0; i < 6; i++){
        if(animal.info.startDate){
          if(todayMonth === new Date(animal.info.startDate).getMonth() + 1 && todayYear === new Date(animal.info.startDate).getFullYear()){
            meses[i][1]++;
          }
        }

        if(animal.info.endDate){
          if(todayMonth === new Date(animal.info.endDate).getMonth() + 1 && todayYear === new Date(animal.info.endDate).getFullYear()){
            meses[i][2]++;
          }
        }

        todayMonth--;
        if(todayMonth === 0){
          todayMonth = 12;
          todayYear--;
        }
      }
    });

    meses.reverse();
    return meses;
  }

  componentDidMount() {
    this.props.getLogs(this.props.auth.uid, (logs) => {
      logs = logs.filter(log => log.type === "animal");
      console.log(logs);
      this.setState({ logs });
    });

    this.props.getAnimals(this.props.auth.uid, (animals) => {
      const groups = this.calculateGroups(animals);
      const meses = this.calculateMonths(animals);

      window.google.charts.load("current", {"packages":["corechart"]});
      window.google.charts.setOnLoadCallback(() => drawChart(groups, meses));

      function drawChart(groups, meses) {
        groups.race.unshift(["Raça", "Quantidade"]);
        meses.unshift(["Mês", "Entradas", "Saídas"]);
        var data = window.google.visualization.arrayToDataTable(groups.race);

        var options = {
          title: "Raças dos animais",
          style: { margin: 0, padding: 0 },
          chartArea: { width: "80%", height: "80%" },
          height: 300
        };

        var chart = new window.google.visualization.PieChart(document.getElementById("piechart"));
        chart.draw(data, options);

        groups.gender.unshift(["Sexo", "Quantidade"]);
        var data2 = window.google.visualization.arrayToDataTable(groups.gender);

        var options2 = {
          title: "Sexos dos animais",
          style: { margin: 0, padding: 0 },
          chartArea: { width: "80%", height: "80%" },
          height: 300
        };

        var chart2 = new window.google.visualization.PieChart(document.getElementById("piechart2"));
        chart2.draw(data2, options2);

        var data3 = window.google.visualization.arrayToDataTable(meses);

        var options3 = {
          title: "Entradas/Saídas de Animais",
          curveType: "function",
          legend: { position: "bottom" },
          chartArea: { width: "80%", height: "80%" }
        };

        var chart3 = new window.google.visualization.LineChart(document.getElementById("linechart"));
        chart3.draw(data3, options3);
      }
    });
  }

  render() {
    const { groups, logs } = this.state;

    return (
      <div className="animalsDashboard">
        <div className="row">
          <div className="col s12 m3">
            <div className="row">
            	<div className="col s12">
                <div className="statContainer green">
                  <div className="row">
                    <div className="col s6">
                      <i className="material-icons md-36">pets</i>
                    </div>
                    <div className="col s6">
                      <div className="row">
                        <div className="col s12">
                          <div className="statNumber">{ groups.type.length > 0 ? groups.type[0][1] + groups.type[1][1] : 0 }</div>
                        </div>
                        <div className="col s12">
                          <div className="unitText">Animais</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col s12">
                <div className="statContainer red darken-3">
                  <div className="row">
                    <div className="col s6">
                      <img src={ cat } width="36px" alt="Cat" />
                    </div>
                    <div className="col s6">
                      <div className="row">
                        <div className="col s12">
                          <div className="statNumber">
                            { groups.type.length > 0 ? groups.type.find(type => type[0] === "Gato")[1] : 0 }
                          </div>
                        </div>
                        <div className="col s12">
                          <div className="unitText">Gatos</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col s12">
                <div className="statContainer yellow darken-3">
                  <div className="row">
                    <div className="col s6">
                      <img src={ dog } width="36px" alt="Dog" />
                    </div>
                    <div className="col s6">
                      <div className="row">
                        <div className="col s12">
                          <div className="statNumber">
                            { groups.type.length > 0 ? groups.type.find(type => type[0] === "Cão")[1] : 0 }
                          </div>
                        </div>
                        <div className="col s12">
                          <div className="unitText">Cães</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/*
              <div className="col m12">
                <div className="statContainer purple lighten-2">
                  <div className="row">
                    <div className="col m6">
                      <img src={ instagram } width="36px" alt="Instagram" />
                    </div>
                    <div className="col m6">
                      <div className="row">
                        <div className="col m12">
                          <div className="statNumber">206</div>
                        </div>
                        <div className="col m12">
                          <div className="unitText">Followers</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col m12">
                <div className="statContainer blue darken-4">
                  <div className="row">
                    <div className="col m6">
                      <img src={ facebook } width="36px" alt="Facebook" />
                    </div>
                    <div className="col m6">
                      <div className="row">
                        <div className="col m12">
                          <div className="statNumber">234</div>
                        </div>
                        <div className="col m12">
                          <div className="unitText">Likes</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              */}
            </div>
          </div>
          <div className="col s12 m9">
            <div className="row">
              <div className="col s12 l6 chart">
                <div id="piechart"></div>
              </div>
              <div className="col s12 l6 log">
                <h5>Notificações</h5>
                <ul>
                  {logs.length > 0 && logs.map((log, i) => {
              			return <li style={{ listStyleType: "square" }} key={ i }>{ log.message }</li>
              		})}
                </ul>
              </div>
              <div className="col s12 l6 chart">
                <div id="piechart2"></div>
              </div>
              <div className="col m12 chart">
                <div id="linechart"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    animals: state.animal.animals
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getAnimals: (userId, callback) => dispatch(getAnimals(userId, callback)),
    getLogs: (userId, callback) => dispatch(getLogs(userId, callback)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
