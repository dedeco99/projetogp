import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { DropzoneArea } from "material-ui-dropzone";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import axios from "axios";

import MaterialDatePicker from "../.partials/DatePicker";

import { getAnimal, createAnimal, editAnimal, resetRedirect } from "../../store/actions/animalActions";

import twitter from "../../img/twitter.png";
import twitterBorder from "../../img/twitterBorder.png";
import instagram from "../../img/instagram.png";
import instagramBorder from "../../img/instagramBorder.png";

class Edit extends Component {
  constructor(props){
    super(props);

    this.state = {
      name: "",
      type: { label: "", value: "" },
      gender: { label: "", value: "" },
      description: "",
      birthDate: null,
      startDate: null,
      endDate: null,
      race: { label: "", value: "" },
      castrated: false,
      forAdoption: false,
      forSponsor: false,
      postOnTwitter: false,
      postOnInstagram: false,
      images: [],
      files: [],

      types: [
        { label: "Gato", value: "cat" },
        { label: "Cão", value: "dog" }
      ],
      genders: [
        { label: "Masculino", value: "male" },
        { label: "Feminino", value: "female" }
      ],

      frontError: "",
      openDialog: false,
      identifiedRace: "",
      dialogAction: () => {},
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleFileUpload = this.handleFileUpload.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFiles = this.handleFiles.bind(this);
    this.deleteImage = this.deleteImage.bind(this);
  }

  componentDidMount() {
    if(this.props.match.params.animal !== "new"){
      this.props.getAnimal(this.props.match.params.animal, this.props.auth.uid, (animalEdit) => {
        this.setState({
          name: animalEdit.info.name,
          type: animalEdit.info.type,
          description: animalEdit.info.description,
          images: animalEdit.info.images,
          gender: animalEdit.info.gender,
          birthDate: animalEdit.info.birthDate,
          startDate: animalEdit.info.startDate,
          endDate: animalEdit.info.endDate,
          race: animalEdit.info.race,
          castrated: animalEdit.info.castrated,
          forAdoption: animalEdit.info.forAdoption,
          forSponsor: animalEdit.info.forSponsor,
        });
      });
    }
  }

  handleChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit(e) {
    e.preventDefault();

    const { name, description, type, gender, birthDate, startDate } = this.state;

    if(name !== "" && description !== "" && type.value !== "" &&
        gender.value !== "" && birthDate !== null && startDate !== null){
      this.validateDates(() => {
        this.handleFileUpload(() => {
          if(this.props.match.params.animal === "new"){
            this.props.createAnimal(this.state, this.props.auth.uid);
          }else{
            this.props.editAnimal(this.props.match.params.animal, this.state, this.props.auth.uid);
          }
        });
      });
    }else{
      this.setState({ frontError: "Preencha todos os campos obrigatórios assinalados com *" });
    }
  }

  validateDates(callback){
    const { startDate, endDate, birthDate } = this.state;

    if(birthDate > startDate){
      this.setState({ frontError: "A data de nascimento não pode ser maior que a data de entrada" });
    }else if(startDate > endDate){
      this.setState({ frontError: "A data de entrada não pode ser maior que a data de saída" });
    }else{
      callback();
    }
  }

  async classifyCat(img, callback){
    let classifier = window.ml5.imageClassifier("MobileNet", this.modelReady);
    await classifier.predict(this.parseImg(img), (err, results) => {
      callback(this.parseResponse(err, results));
    });
  }

  parseResponse(err,results){
    var race = results[0].className;

    switch(race) {
      case "tabby, tabby cat":
        return "Tabby";
      case "tiger cat":
        return "Tabby";
      case "Persian cat":
        return "Persian";
      case "Siamese cat, Siamese":
        return "Siamese";
      case "Egyptian cat":
        return "Bombay";
      default:
        return "Unknown";
    }
  }

  modelReady(){
    console.log("MobileNet Model Loaded");
  }

  imgElem(imgPath){
    var img=document.createElement("img");
    img.src=imgPath;
    //resize
    img.height=420;
    img.width=420;
    return img;
  }

  getBase64(file, callback) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      callback(reader.result);
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  }

  parseImg(img){
    if(img instanceof HTMLImageElement){
      return img;
    }else{
      return this.imgElem(img);
    }
  }

  handleFileUpload(callback) {
    let filesUploaded = 0;

    const { type, name, gender, postOnTwitter, postOnInstagram, files } = this.state;

    if(files.length > 0){
      files.forEach((file) => {
        const formData = new FormData();
        formData.append("fileToUpload", file);
        const config = {
            headers: {
                "content-type": "multipart/form-data"
            }
        };

        axios.post("/api/animals/" + type.value + "/photos?"
        + "postOnTwitter=" + postOnTwitter + "&postOnInstagram=" + postOnInstagram
        + "&name=" + name + "&type=" + type.label + "&gender=" + gender.label
        , formData, config)
        .then((response) => {
          if(!response.data.error){
            this.state.images.push(response.data.link);
          }

          filesUploaded++;

          if(filesUploaded === files.length){
            if(type.value === "cat"){
              this.getBase64(file, (base) => {
                this.classifyCat(base, (res) => {
                  this.openDialog({ type: type.value, race: res });
                });
              })
            }else{
              this.openDialog({ type: type.value, race: response.data.breed });
            }
          }
        }).catch((error) => {
          console.log(error);
        });
      });
    }else{
      callback();
    }
  }

  handleFiles(files) {
    this.setState({ files });
  }

  deleteImage(e) {
    let image = e.target.id;
    let images = this.state.images.filter((value) => {
      return value !== image;
    });

    this.setState({ images });
  }

  openDialog(res) {
    this.setState({ files: [] });
    if(res.type === "dog" && res.race !== "Not a dog"){
      this.setState({
        openDialog: true,
        identifiedRace:  { label: res.race, value: res.race },
        dialogAction: () => {
          this.setState({ race: { label: res.race, value: res.race } });
        }
      });
    }else if(res.type === "cat" && res.race !== "Unknown"){
      this.setState({
        openDialog: true,
        identifiedRace: { label: res.race, value: res.race },
        dialogAction: () => {
          this.setState({ race: { label: res.race, value: res.race } });
        }
      });
    }else{
      if(this.props.match.params.animal === "new"){
        this.props.createAnimal(this.state, this.props.auth.uid);
      }else{
        this.props.editAnimal(this.props.match.params.animal, this.state, this.props.auth.uid);
      }
    }
  }

  render() {
    const { redirect, resetRedirect } = this.props;
    const { name, type, description, images, gender, birthDate, startDate, endDate, race, castrated, forAdoption, forSponsor, postOnTwitter, postOnInstagram, frontError, genders, types, openDialog, identifiedRace, dialogAction } = this.state;
    const { from } = this.props.location.state || { from: { pathname: "/animais" } };

    if (redirect){
      resetRedirect();
      return <Redirect to={ from } />;
    }

    const imagePreviews = images.map((image) => {
      return (
        <div className="col s6 m3 l2 xl2" key={ image }>
          <div className="imageContainer">
            <div className="deleteImage" id={ image } onClick={ this.deleteImage }>
              <i className="material-icons" id={ image }>delete</i>
            </div>
            <img src={ image } width="100px" height="80px" alt=""/>
          </div>
        </div>
      );
    });

    return (
      <div className="container">
        <form onSubmit={ this.handleSubmit }>
          <h5 className="grey-text text-darken-3">
            <Link id="goBack" to="/animais">
              <i className="material-icons">keyboard_arrow_left</i>
            </Link>
            Animal
          </h5>
          <br/>
          <div className="row">
            <div className="col s12">
              <TextField
                id="name"
                label="Nome"
                value={ name }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <TextField
                multiline
                id="description"
                label="Descrição"
                value={ description }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12 m6 l4">
              <TextField
                select
                id="type"
                label="Tipo de animal"
                value={ type.value + "" }
                onChange={ (e, value) => this.setState({ type: { label: value.props.children, value: value.props.value} }) }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              >
                {types.map(type => (
                  <MenuItem key={type.value} value={type.value}>
                    {type.label}
                  </MenuItem>
                ))}
              </TextField>
            </div>
            <div className="col s12 m6 l4">
              <TextField
                select
                id="gender"
                label="Sexo"
                value={ gender.value + "" }
                onChange={ (e, value) => this.setState({ gender: { label: value.props.children, value: value.props.value} }) }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              >
                {genders.map(gender => (
                  <MenuItem key={gender.value} value={gender.value}>
                    {gender.label}
                  </MenuItem>
                ))}
              </TextField>
            </div>
            <div className="col s12 m6 l4">
              <MaterialDatePicker
                date={ birthDate }
                label="Data de Nascimento"
                setDate={ date => this.setState({ birthDate: date.format("YYYY-MM-DD") }) }
                required={ true }
              />
            </div>
            <div className="col s12 m6 l4">
              <MaterialDatePicker
                date={ startDate }
                label="Data de Entrada"
                setDate={ date => this.setState({ startDate: date.format("YYYY-MM-DD") }) }
                required={ true }
              />
            </div>
            <div className="col s12 m6 l4">
              <MaterialDatePicker
                date={ endDate }
                label="Data de Saída"
                setDate={ date => this.setState({ endDate: date.format("YYYY-MM-DD") }) }
                required={ false }
              />
            </div>
            <div className="col s12 m6 l4" align="center">
              <FormControlLabel
                control={
                  <Checkbox
                    id="castrated"
                    color="default"
                    onChange={ e => this.setState({ castrated: e.target.checked }) }
                    checked={ castrated }
                  />
                }
                label="Castrado"
                style={{ padding: "20px 0px" }}
              />
            </div>
            <div className="col s12 m6 l4" align="center">
              <FormControlLabel
                control={
                  <Checkbox
                    id="forAdoption"
                    color="default"
                    onChange={ e => this.setState({ forAdoption: e.target.checked }) }
                    checked={ forAdoption }
                  />
                }
                label="Adoção"
                style={{ padding: "20px 0px" }}
              />
            </div>
            <div className="col s12 m6 l4" align="center">
              <FormControlLabel
                control={
                  <Checkbox
                    id="forSponsor"
                    color="default"
                    onChange={ e => this.setState({ forSponsor: e.target.checked }) }
                    checked={ forSponsor }
                  />
                }
                label="Apadrinhamento"
                style={{ padding: "20px 0px" }}
              />
            </div>
            <div className="col s12 m6 l4" align="center">
              <FormControlLabel
                control={
                  <Checkbox
                    id="postOnTwitter"
                    color="default"
                    icon={<img src={twitterBorder} width="30px" />}
                    checkedIcon={<img src={twitter} width="30px" />}
                    onChange={ e => this.setState({ postOnTwitter: e.target.checked }) }
                    checked={ postOnTwitter }
                  />
                }
                label="Post on Twitter"
                style={{ padding: "20px 0px" }}
              />
            </div>
            <div className="col s12 m6 l4" align="center">
              <FormControlLabel
                control={
                  <Checkbox
                    id="postOnInstagram"
                    color="default"
                    icon={<img src={instagramBorder} width="30px" />}
                    checkedIcon={<img src={instagram} width="30px" />}
                    onChange={ e => this.setState({ postOnInstagram: e.target.checked }) }
                    checked={ postOnInstagram }
                  />
                }
                label="Post on Instagram"
                style={{ padding: "20px 0px" }}
              />
            </div>
            <div className="col s12">
              <TextField
                id="race"
                label="Raça"
                value={ race.value }
                onChange={ (e) => this.setState({ race: { label: e.target.value, value: e.target.value} }) }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
              />
            </div>
            <div className="col s12">
              <br />
              <div className="row">
                { imagePreviews }
              </div>
            </div>
            <div className="col s12">
              <div style={{ margin: "20px", color: "#777" }}>
                <DropzoneArea
                  onChange={ this.handleFiles }
                  dropzoneText={ "Arraste imagens para aqui ou clique" }
                  showAlerts={ false }
                />
              </div>
            </div>
            <div className="col s12">
              <div className="red-text center">{ frontError }</div>
              <br />
            </div>
            <div className="col s12">
              <div className="center">
                <button className="btn red darken-1">Guardar</button>
              </div>
            </div>
          </div>
        </form>
        <Dialog
          open={ openDialog }
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Raça identificada</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Identificámos que a raça do seu animal talvez seja { identifiedRace.value }
              <br />
              Quer alterar no formulário?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={ () => this.setState({ openDialog: false }) }
              className="button"
            >
              Cancelar
            </Button>
            <Button
              onClick={ () => {
                dialogAction();
                this.setState({ openDialog: false });
              } }
              className="button"
              autoFocus
            >
              Confirmar
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    redirect: state.animal.redirect
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAnimal: (animalId, userId, callback) => dispatch(getAnimal(animalId, userId, callback)),
    createAnimal: (animal, userId) => dispatch(createAnimal(animal, userId)),
    editAnimal: (animalId, animal, userId) => dispatch(editAnimal(animalId, animal, userId)),
    resetRedirect: () => dispatch(resetRedirect())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
