import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";
import Checkbox from "@material-ui/core/Checkbox";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Badge from "@material-ui/core/Badge";
import Tooltip from '@material-ui/core/Tooltip';

import Categories from "../.partials/Categories";
import Posts from "./Posts";

import { getAnimals } from "../../store/actions/animalActions";
import { getContent } from "../../store/actions/contentActions";

import "../../css/Animals.css";

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

class Animals extends Component {
  constructor(props){
    super(props);

    this.state = {
      open: true,
      forAdoption: false,
      forSponsor: false,
      search: "",
      orderable: "random",
      orderables: [
        { value: "random" , label: "Não Ordenado" },
        { value: "name" , label: "Nome" },
        { value: "type" , label: "Tipo de Animal" },
        { value: "gender" , label: "Género" },
        { value: "age" , label: "Idade" },
        { value: "race" , label: "Raça" }
      ],
      asc: true,
      type: "all",
      types: [],
      gender: "all",
      genders: [],
      race: "all",
      races: [],
      ageRange: [0, 20],
      castrated: false,
      groupable: "type",
      groupables: [
        { value: "type" , label: "Tipo de Animal" },
        { value: "gender" , label: "Género" },
        { value: "race" , label: "Raça" }
      ],
    }

    this.setCategory = this.setCategory.bind(this);
  }

  componentDidMount() {
    this.props.getContent(this.props.auth.uid);

    this.props.getAnimals(this.props.auth.uid, (animals) => {
      let types = [], genders = [], races = [];

      types.push({ value: "all", label: "Todos" });
      genders.push({ value: "all", label: "Todos" });
      races.push({ value: "all", label: "Todos" });

      animals.forEach(animal => {
        if(!types.find(type => type.value === animal.info.type.value)){
          types.push(animal.info.type);
        }

        if(!genders.find(gender => gender.value === animal.info.gender.value)){
          genders.push(animal.info.gender);
        }

        if(!races.find(race => race.value === animal.info.race.value)){
          let race = animal.info.race;
          if(race.value === "") race.label = "Sem raça";

          races.push(race);
        }
      });

      this.setState({ types, genders, races });
    });

    var elems = document.querySelectorAll(".tabs");
    window.M.Tabs.init(elems);

    elems = document.querySelectorAll(".modal");
    window.M.Modal.init(elems);
  }

  setCategory(value) {
    if(value === "forAdoption"){
      this.setState({ forAdoption: true, forSponsor: false });
    }else if(value === "forSponsor"){
      this.setState({ forAdoption: false, forSponsor: true });
    }else{
      this.setState({ forAdoption: false, forSponsor: false });
    }
  }

  filter(e) {
    const { search, type, gender, race, castrated, ageRange, forAdoption, forSponsor } = this.state;
    const { animals } = this.props;

    let res = [];

    animals.forEach(animal => {
      if(
        (animal.info.type.value === type || type === "all") &&
        (animal.info.gender.value === gender || gender === "all") &&
        (animal.info.race.value === race || race === "all") &&
        (animal.info.castrated === castrated || castrated === false) &&
        (this.calculateAge(animal.info.birthDate) >= ageRange[0] && this.calculateAge(animal.info.birthDate) <= ageRange[1]) &&
        (animal.info.forAdoption === forAdoption || forAdoption === false) &&
        (animal.info.forSponsor === forSponsor || forSponsor === false)
      ){
        if(
          animal.info.name.toLowerCase().includes(search.toLowerCase()) ||
          animal.info.type.label.toLowerCase().includes(search.toLowerCase()) ||
          animal.info.gender.label.toLowerCase().includes(search.toLowerCase()) ||
          animal.info.race.label.toLowerCase().includes(search.toLowerCase()) ||
          animal.info.description.toLowerCase().includes(search.toLowerCase())
        ){
          res.push(animal);
        }
      }
    });

    return this.order(res);
  }

  order(animals) {
    const { orderable, asc } = this.state;

    if(orderable === "random"){
      return animals;
    }else if(orderable === "name"){
      animals.sort((a, b) => {
        if(a.info.name.toLowerCase() < b.info.name.toLowerCase())
          return -1;
        if(a.info.name.toLowerCase() > b.info.name.toLowerCase())
          return 1;
        return 0;
      });
    }else if(orderable === "age"){
      animals.sort((a, b) => {
        if(this.calculateAge(a.info.birthDate) < this.calculateAge(b.info.birthDate))
          return -1;
        if(this.calculateAge(a.info.birthDate) > this.calculateAge(b.info.birthDate))
          return 1;
        return 0;
      });
    }else{
      animals.sort((a, b) => {
        if(a.info[orderable].label < b.info[orderable].label)
          return -1;
        if(a.info[orderable].label > b.info[orderable].label)
          return 1;
        return 0;
      });
    }

    if(!asc){
      animals.reverse();
    }

    return animals;
  }

  calculateAge(birthDate) {
    var ageDifMs = Date.now() - new Date(birthDate).getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  filterGroup(group) {
    const { groupable } = this.state;

    switch(groupable){
      case "type":
        this.setState({ type: group, gender: "all", race: "all" });
        return;
      case "gender":
        this.setState({ type: "all", gender: group, race: "all" });
        return;
      case "race":
        this.setState({ type: "all", gender: "all", race: group });
        return;
      default:
        return;
    }
  }

  calculateGroups() {
    const { groupable } = this.state;
    const { animals } = this.props;

    let groups = [];

    animals.forEach(animal => {
      let groupExists = false;

      if(groups.length > 0){
        groups.forEach(group => {
          if(group.name.value === animal.info[groupable].value){
            group.value++;
            groupExists = true;
            return;
          }
        });
      }

      if(groups.length === 0 || !groupExists){
        groups.push({ name: animal.info[groupable], value: 1 });
      }
    });

    return groups;
  }

  renderGroups() {
    const groups = this.calculateGroups();

    const groupsList = groups.length>0 ? (
  		groups.map(group => {
  			return (
          <ListItem
            onClick={ () => this.filterGroup(group.name.value) }
            style={{ borderBottom: "1px solid lightgrey", color: "black", cursor: "pointer" }}
            key={ group.name.value }
          >
            { group.name.label || "Sem raça" }
            <Badge
              color="error"
              badgeContent={ group.value }
              children={""}
              style={{ right: 25, position: "absolute" }}
            />
          </ListItem>
  			)
  		})
  	) : (
  		""
  	)
    return(
      <div>
        { groupsList }
      </div>
    );
  }

  getContent(search){
    const { content } = this.props;
    return content.length > 0 && content.find(c => c.info.page === search).info.content;
  }

  render(){
    const categories = [
      {id: "all", displayName: "Todos", path: "/animais", active: true },
      {id: "forAdoption", displayName: "Adoção", path: "/animais", active: false },
      {id: "forSponsor", displayName: "Apadrinhamento", path: "/animais", active: false }
    ];

    const { animals, content } = this.props;
    const { open, search, orderables, orderable, asc, type, types, gender, genders, race, races, ageRange, castrated, groupable, groupables } = this.state;

    return (
      <div className="animals">
        <Categories
          options={ categories }
          idField="id"
          nameField="displayName"
          action={ this.setCategory }
        />
        <br/>
        <Grid container spacing={16}>
          <Grid item xs={2} sm={1}>
            <IconButton
              className={ open ? "action expand" : "action expandOpen" }
              aria-label="Open"
              onClick={() => this.setState({ open: !open })}
              style={{ margin: "18px 0px" }}
            >
              <i id="closeSidebar" className="material-icons">keyboard_arrow_left</i>
            </IconButton>
          </Grid>
          <Grid item xs={10} sm={8}>
            <TextField
              id="search"
              label="Pesquisar"
              value={ search }
              onChange={ (e) => this.setState({ search: e.target.value }) }
              InputProps={{
                classes: { notchedOutline: "input" },
                startAdornment: <InputAdornment position="start"><i className="material-icons prefix">search</i></InputAdornment>,
              }}
              margin="normal"
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item sm={3} align="center">
            <Tooltip title={ this.getContent("tooltipAdd") } placement="top">
              <Link
                to="/animais/new"
                className="addAnimal btn-floating waves-effect waves-light green darken-2"
                style={{ marginTop: "20px", marginLeft: "10px" }}
              >
                <i className="material-icons">add</i>
              </Link>
            </Tooltip>
            <Tooltip title={ this.getContent("tooltipDashboard") } placement="top">
              <Link
                to="/animais/dashboard"
                className="addAnimal btn-floating waves-effect waves-light orange darken-2"
                style={{ marginTop: "20px", marginLeft: "10px" }}
              >
                <i className="material-icons">dashboard</i>
              </Link>
            </Tooltip>
          </Grid>
        </Grid>
        <Grid container spacing={16}>
          <Grid
            item
            xs={open ? 12 : false} sm={open ? 4 : false} md={open ? 3 : false} lg={open ? 2 : false}
            style={open ? {} : { display: "none" }}
          >
            <List style={{ border: "1px solid lightgrey", borderRadius: 5, padding: 0 }}>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <h6 style={{ textAlign: "center", width: "100%", color: "black" }}>
                  <b>Filtros</b>
                </h6>
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <Grid container spacing={0}>
                  <Grid item xs={10}>
                    <TextField
                      select
                      id="orderable"
                      label="Ordenar"
                      value={ orderable }
                      onChange={ (e, value) => this.setState({ orderable: e.target.value }) }
                      InputProps={{ classes: { notchedOutline: "input" } }}
                      margin="normal"
                      variant="outlined"
                      fullWidth
                    >
                      {orderables.map(orderable => (
                        <MenuItem key={orderable.value} value={orderable.value}>
                          {orderable.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  <Grid item xs={2}>
                    <IconButton
                      className={ asc ? "action expand" : "action expandOpen" }
                      aria-label="AscDesc"
                      onClick={() => this.setState({ asc: !asc })}
                      style={{ margin: "18px 0px" }}
                    >
                      <i id="closeSidebar" className="material-icons">arrow_downward</i>
                    </IconButton>
                  </Grid>
                </Grid>
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <TextField
                  select
                  id="type"
                  label="Tipo de Animal"
                  value={ type }
                  onChange={ (e, value) => this.setState({ type: e.target.value }) }
                  InputProps={{ classes: { notchedOutline: "input" } }}
                  margin="normal"
                  variant="outlined"
                  fullWidth
                >
                  {types.map(type => (
                    <MenuItem key={type.value} value={type.value}>
                      {type.label}
                    </MenuItem>
                  ))}
                </TextField>
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <TextField
                  select
                  id="gender"
                  label="Sexo"
                  value={ gender }
                  onChange={ (e, value) => this.setState({ gender: e.target.value }) }
                  InputProps={{ classes: { notchedOutline: "input" } }}
                  margin="normal"
                  variant="outlined"
                  fullWidth
                >
                  {genders.map(gender => (
                    <MenuItem key={gender.value} value={gender.value}>
                      {gender.label}
                    </MenuItem>
                  ))}
                </TextField>
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <TextField
                  select
                  id="race"
                  label="Raça"
                  value={ race }
                  onChange={ (e, value) => this.setState({ race: e.target.value }) }
                  InputProps={{ classes: { notchedOutline: "input" } }}
                  margin="normal"
                  variant="outlined"
                  fullWidth
                >
                  {races.map(race => (
                    <MenuItem key={race.value} value={race.value}>
                      {race.label}
                    </MenuItem>
                  ))}
                </TextField>
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <FormControlLabel
                  control={
                    <Range
                      id="ageRange"
                      value={ ageRange }
                      max={20}
                      onChange={ (value) => this.setState({ ageRange: value }) }
                    />
                  }
                  label="Idade"
                  labelPlacement="top"
                  style={{ width: "100%" }}
                />
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      id="castrated"
                      color="default"
                      onChange={ e => this.setState({ castrated: e.target.checked }) }
                      checked={ castrated }
                    />
                  }
                  label="Castrado"
                />
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <TextField
                  select
                  id="groupable"
                  label="Agrupar"
                  value={ groupable }
                  onChange={ (e, value) => this.setState({ groupable: e.target.value }) }
                  InputProps={{ classes: { notchedOutline: "input" } }}
                  margin="normal"
                  variant="outlined"
                  fullWidth
                >
                  {groupables.map(groupable => (
                    <MenuItem key={groupable.value} value={groupable.value}>
                      {groupable.label}
                    </MenuItem>
                  ))}
                </TextField>
              </ListItem>
              { this.renderGroups() }
            </List>
          </Grid>
          <Grid
            item
            xs={open ? 12 : false} sm={open ? 8 : false} md={open ? 9 : false} lg={open ? 10 : false}
          >
            <Posts posts={ this.filter(animals) } />
          </Grid>
        </Grid>
        <div id="modalImage" className="modal">
  		    <div className="modal-content">
  		      <img id="modalImageSrc" src="" width="100%" alt="Animal fofinho" />
  		    </div>
  		  </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    animals: state.animal.animals,
    content: state.content.content
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getAnimals: (userId, callback) => dispatch(getAnimals(userId, callback)),
    getContent: (userId) => dispatch(getContent(userId)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Animals);
