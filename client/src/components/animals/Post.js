import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Tooltip from '@material-ui/core/Tooltip';

import { deleteAnimal } from "../../store/actions/animalActions";
import { getContent } from "../../store/actions/contentActions";

import cat from "../../img/cat.png";
import dog from "../../img/dog.png";
import male from "../../img/male.png";
import female from "../../img/female.png";

class Post extends Component {
  state = {
    expanded: false,
    openDialog: false,
  }

  componentDidMount() {
    this.props.getContent(this.props.auth.uid);
  }

  openImage = (e) => {
		var modalImageSrc = document.getElementById("modalImageSrc");
		modalImageSrc.src = e.target.firstChild.src;
		console.log(e.target.firstChild.src);
		var elem = document.getElementById("modalImage");
    var instance = window.M.Modal.getInstance(elem);
		instance.open();
	}

  handleExpandClick = () => {
    this.setState({ expanded: !this.state.expanded });
  };

  deleteAnimal = () => {
    const { post, auth, deleteAnimal } = this.props;

    deleteAnimal(post.id, auth.uid);
  }

  getContent(search){
    const { content } = this.props;
    return content.length > 0 && content.find(c => c.info.page === search).info.content;
  }

  renderLinks = () => {
    const { post, auth } = this.props;

    return auth.uid ? (
      <div>
        <Link to={ "/animais/" + post.id }>
          <Tooltip title={ this.getContent("tooltipEdit") } placement="top">
            <IconButton
              className="action"
              aria-label="Edit"
            >
              <i className="material-icons">edit</i>
            </IconButton>
          </Tooltip>
        </Link>
        <Tooltip title={ this.getContent("tooltipDelete") } placement="top">
          <IconButton
            className="action"
            onClick={ () => this.setState({ openDialog: true }) }
            aria-label="Delete"
          >
            <i className="material-icons">delete</i>
          </IconButton>
        </Tooltip>
        <Link to={ "/animais/" + post.id + "/eventos" }>
          <Tooltip title={ this.getContent("tooltipEvents") } placement="top">
            <IconButton
              className="action"
              aria-label="Event"
            >
              <i className="material-icons">event</i>
            </IconButton>
          </Tooltip>
        </Link>
        <Link to={ "/animais/BcNqOKp0MJxgAMblWR3G/" + post.id }>
          <Tooltip title={ this.getContent("tooltipAdopt") } placement="top">
            <IconButton
              className="action"
              aria-label="Adopt"
            >
              <i className="material-icons">pets</i>
            </IconButton>
          </Tooltip>
        </Link>
        <Link to={ "/animais/TAtAEhFAFkkRxGYyygrR/" + post.id }>
          <Tooltip title={ this.getContent("tooltipSponsor") } placement="top">
            <IconButton
              className="action"
              aria-label="Sponsor"
            >
              <i className="material-icons">euro_symbol</i>
            </IconButton>
          </Tooltip>
        </Link>
      </div>
    ) : (
      <div>
        <Link to={ "/animais/BcNqOKp0MJxgAMblWR3G/" + post.id }>
          <IconButton
            className="action"
            aria-label="Adopt"
          >
            <i className="material-icons">pets</i>
          </IconButton>
        </Link>
        <Link to={ "/animais/TAtAEhFAFkkRxGYyygrR/" + post.id }>
          <IconButton
            className="action"
            aria-label="Sponsor"
          >
            <i className="material-icons">euro_symbol</i>
          </IconButton>
        </Link>
      </div>
    );
  }

  render(){
    const { post, auth } = this.props;
    const { expanded, openDialog } = this.state;
    const links = this.renderLinks();

    return (
      <Card className="red darken-3">
        <CardHeader
          title={ <span style={{ color: "white" }}>{ post.info.name }</span> }
          subheader={ <span style={{ color: "white" }}>{ post.info.race.value }</span> }
          action={
            <span className="animalAttributes">
              {
                post.info.type.value === "cat"
                ? <img src={ cat } width="40px" alt="Cat" />
                : <img src={ dog } width="40px" alt="Dog" />
              }
              {
                post.info.gender.value === "male"
                ? <img src={ male } width="40px" alt="Male" />
                : <img src={ female } width="40px" alt="Female" />
              }
            </span>
          }
        />
        <Carousel
          showArrows={ true }
          showStatus={ false }
          showThumbs={ false }
          infiniteLoop={ true }
        >
          {post.info.images.length > 0
            ? post.info.images.map((image, i) => (
              <div key={ i } onClick={ this.openImage }>
                <img src={ image } alt="" />
              </div>
            ))
            : ""
          }
        </Carousel>
        <CardContent>
          <Typography component="p" className="white-text center">
            { post.info.description }
          </Typography>
        </CardContent>
        <CardActions>
          { links }
          <IconButton
            className={ expanded ? "action expandOpen" : "action expand" }
            onClick={ this.handleExpandClick }
            aria-expanded={ expanded }
            aria-label="Show more"
          >
            <i className="material-icons">expand_more</i>
          </IconButton>
        </CardActions>
        <Collapse in={ expanded } timeout="auto" unmountOnExit>
          <List>
            <ListItem>
              <Avatar style={{ backgroundColor: "darkgrey" }}>
                <i className="material-icons circle">date_range</i>
              </Avatar>
              <ListItemText
                classes={{ primary: "white-text", secondary: "white-text" }}
                primary="Data de Nascimento"
                secondary={ post.info.birthDate }
              />
            </ListItem>
            <ListItem>
              <Avatar style={{ backgroundColor: "darkgrey" }}>
                <i className="material-icons circle">child_friendly</i>
              </Avatar>
              <ListItemText
                classes={{ primary: "white-text", secondary: "white-text" }}
                primary="Castrado"
                secondary={
                  post.info.castrated
                  ? <i className="material-icons">done</i>
                  : <i className="material-icons">clear</i>
                }
              />
            </ListItem>
          </List>
        </Collapse>
        <Dialog
          open={ openDialog }
          onClose={ () => this.setState({ openDialog: false }) }
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Apagar animal</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Tem a certeza que quer apagar este animal?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={ () => this.setState({ openDialog: false }) }
              className="button"
            >
              Cancelar
            </Button>
            <Button
              onClick={ () => {
                this.deleteAnimal(post.id, auth.uid);
                this.setState({ openDialog: false });
              } }
              className="button"
              autoFocus
            >
              Confirmar
            </Button>
          </DialogActions>
        </Dialog>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    content: state.content.content
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    deleteAnimal: (animal, userId) => dispatch(deleteAnimal(animal, userId)),
    getContent: (userId) => dispatch(getContent(userId)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Post);
