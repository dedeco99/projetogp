import React from "react";
import { NavLink } from "react-router-dom";

const LoggedOutLinks = () => {
  return (
    <div className="loggedOutLinks">
      <ul className="left">
        <li className="sliding-middle-out">
          <NavLink to="/animais">Animais</NavLink>
        </li>
      </ul>
      <ul className="right">
        <li className="sliding-middle-out">
          <NavLink to="/login">Login</NavLink>
        </li>
      </ul>
    </div>
  )
}

export default LoggedOutLinks
