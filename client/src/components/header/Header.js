import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import LoggedInLinks from "./LoggedInLinks";
import LoggedOutLinks from "./LoggedOutLinks";

import "../../css/Header.css";
import logo from "../../img/logo.png";

const Header = (props) => {
  const { auth } = props;
  const links = auth.uid ? <LoggedInLinks /> : <LoggedOutLinks />;

  return (
    <div className="header">
      <nav>
        <div className="nav-wrapper red darken-3">
          <Link to="/" className="navbar-logo">
            <img src={ logo } id="logo" width="50px" alt="Logo"/>
          </Link>
    			{ links }
    		</div>
      </nav>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth
  }
}

export default connect(mapStateToProps)(Header);
