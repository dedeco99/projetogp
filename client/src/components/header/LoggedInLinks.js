import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";

import { getUsers } from "../../store/actions/userActions";
import { getContent } from "../../store/actions/contentActions";
import { logout } from "../../store/actions/authActions";

const LoggedInLinks = ({ auth, users, getUsers, content, getContent, logout }) => {
  var isAdmin = () => {
    var user = users.find(user => user.uid === auth.uid);

    if(user && user.customClaims) return user.customClaims.admin;
  };

  if(users.length === 0) getUsers(auth.uid);

  if(content.length === 0) getContent(auth.uid);

  var adminLinks = isAdmin() ? (
    <span>
      <li className="sliding-middle-out">
        <NavLink to="/utilizadores">Utilizadores</NavLink>
      </li>
      <li className="sliding-middle-out">
        <NavLink to="/conteudo">Conteúdo</NavLink>
      </li>
    </span>
  ) : (
    ""
  );

  return (
    <div className="loggedInLinks">
      <ul className="left">
        <li className="sliding-middle-out">
          <NavLink to="/animais">Animais</NavLink>
        </li>
        <li className="sliding-middle-out">
          <NavLink to="/processos">Processos</NavLink>
        </li>
        { adminLinks }
      </ul>
      <ul className="right">
        <li>
          <NavLink to={ "/utilizadores/" + auth.uid }><i className="material-icons">person</i></NavLink>
        </li>
        <li className="sliding-middle-out">
          <NavLink to="/logout" onClick={ logout }>Logout</NavLink>
        </li>
      </ul>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    users: state.user.users,
    content: state.content.content
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logout()),
    getUsers: (userId) => dispatch(getUsers(userId)),
    getContent: (userId) => dispatch(getContent(userId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoggedInLinks)
