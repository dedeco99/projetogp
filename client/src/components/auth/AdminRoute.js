import React from "react";
import { Route } from "react-router-dom";
import { connect } from "react-redux";

import { getUsers } from "../../store/actions/userActions";

const AdminRoute = ({component: Component, ...rest }) => {
  const { auth, users, getUsers } = rest;

  if(users.length === 0) getUsers(auth.uid);

  var isAdmin = () => {
    var user = users.find(user => user.uid === auth.uid);

    if(user && user.customClaims) return user.customClaims.admin;
  };

  return (
    <Route
      {...rest}
      render={props => {
        if(auth.uid && isAdmin()){
        	return <Component {...props} />;
				}else{
	        return <div className="black-text">Não és administrador</div>
				}
      }}
    />
  )
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    users: state.user.users
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getUsers: (userId) => dispatch(getUsers(userId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminRoute);
