import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";

import { login } from "../../store/actions/authActions";

class Login extends Component {
  state = {
    email: "",
    password: ""
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.login(this.state);
  }

  render() {
    const { auth, authError } = this.props;
    const { email, password } = this.state;
    const { from } = this.props.location.state || { from: { pathname: "/" } };

    if (auth.uid) return <Redirect to={ from } />;

    return(
      <div className="container">
        <form onSubmit={ this.handleSubmit }>
          <h5 className="grey-text text-darken-3">Login</h5>
          <br />
          <div className="row">
            <div className="col s12">
              <TextField
                id="email"
                label="Email"
                value={ email }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <TextField
                id="password"
                label="Password"
                type="password"
                value={ password }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12 center">
              <Link to="/login/forgotPassword" className="black-text right">
                <u>Esqueci-me da password</u>
              </Link>
              <br/>
              <button className="btn red darken-1">Login</button>
              <div className="red-text center">
                { authError ? <p>{ authError }</p> : null }
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (credentials) => dispatch(login(credentials))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
