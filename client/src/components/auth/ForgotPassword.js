import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";

import { forgotPassword } from "../../store/actions/authActions";

class ForgotPassword extends Component {
  state = {
    email: ""
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.forgotPassword(this.state.email);
  }

  render() {
    const { forgotPasswordMessage } = this.props;
    const { email } = this.state;

    return(
      <div className="container">
        <form onSubmit={ this.handleSubmit }>
          <h5 className="grey-text text-darken-3">
            <Link id="goBack" to="/login">
              <i className="material-icons">keyboard_arrow_left</i>
            </Link>
            Esqueci-me da password
          </h5>
          <br />
          <div className="row">
            <div className="col s12">
              <TextField
                id="email"
                label="Email"
                value={ email }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <div className="center">
                <div className={ forgotPasswordMessage ? forgotPasswordMessage.color : null }>
                  { forgotPasswordMessage ? <p>{ forgotPasswordMessage.message }</p> : null }
                </div>
                <br />
                <button className="btn red darken-1">Enviar</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    forgotPasswordMessage: state.auth.forgotPasswordMessage
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    forgotPassword: (email) => dispatch(forgotPassword(email))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
