import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Tooltip from '@material-ui/core/Tooltip';

import { getUsers, editUser, deleteUser, setAsAdmin } from "../../store/actions/userActions";
import { getContent } from "../../store/actions/contentActions";

import "../../css/Users.css";

class Users extends Component {
  state = {
    openDialog: false,
    selectedUser: null
  }

  componentDidMount() {
    this.props.getContent(this.props.auth.uid);
  }

  deleteUser = (user) => {
    const { auth, deleteUser } = this.props;

    deleteUser(user, auth.uid)
  }

  setAsAdmin = (e) => {
    this.props.setAsAdmin(e.target.id, this.props.auth.uid)
  }

  getContent(search){
    const { content } = this.props;
    return content.length > 0 && content.find(c => c.info.page === search).info.content;
  }

  render(){
    const { users } = this.props;
    const { openDialog, selectedUser } = this.state;

    const usersList = users.length>0 ? (
  		users.map(user => {
        var setAsAdmin = user.customClaims && user.customClaims.admin ? (
          <td align="center">
            <Link
              to="/utilizadores"
              onClick={ this.setAsAdmin }
              className="btn-floating waves-effect waves-light green darken-2"
            >
              <i id={ user.uid } className="material-icons">check</i>
            </Link>
          </td>
        ) : (
          <td align="center">
            <Link
              to="/utilizadores"
              onClick={ this.setAsAdmin }
              className="btn-floating waves-effect waves-light blue darken-2"
            >
              <i id={ user.uid } className="material-icons">security</i>
            </Link>
          </td>
        );

  			return (
          <tr key={ user.uid }>
            <td>{ user.displayName }</td>
            <td>{ user.email }</td>
            <td>{ new Date(user.metadata.lastSignInTime).toLocaleString("pt-PT") }</td>
            <td>{ new Date(user.metadata.creationTime).toLocaleString("pt-PT") }</td>
            <td align="center">
              <Link
                to={ "/utilizadores/" + user.uid }
                className="btn-floating waves-effect waves-light yellow darken-2"
              >
                <i className="material-icons">edit</i>
              </Link>
            </td>
            <td align="center">
              <Link to="/utilizadores"
                onClick={ () => this.setState({ openDialog: true, selectedUser: user.uid }) }
                className="btn-floating waves-effect waves-light red darken-2"
              >
                <i id={ user.uid } className="material-icons">delete</i>
              </Link>
            </td>
            { setAsAdmin }
          </tr>
  			)
  		})
  	) : (
  		<tr>
  			<td colSpan="6"><div align="center">Não há utilizadores</div></td>
  		</tr>
  	)

    return (
      <div className="users container">
        <div className="row">
          <div className="col m10">
            {/*<div className="input-field col s12">
              <i className="material-icons prefix">search</i>
              <input type="text" id="autocomplete-input" className="autocomplete" />
              <label htmlFor="autocomplete-input">Procurar</label>
            </div>*/}
          </div>
          <div className="col m2">
            <Tooltip title={ this.getContent("tooltipAdd") } placement="top">
              <Link
                to="/utilizadores/new"
                className="addUser btn-floating waves-effect waves-light green darken-2"
              >
                <i className="material-icons">add</i
              ></Link>
            </Tooltip>
          </div>
        </div>
        <table>
          <thead>
            <tr>
              <th>Nome</th>
              <th>Email</th>
              <th>Última Entrada</th>
              <th>Data de Registo</th>
              <th>Editar</th>
              <th>Apagar</th>
              <th>Admin</th>
            </tr>
          </thead>
          <tbody>
            { usersList }
          </tbody>
        </table>
        <Dialog
          open={ openDialog }
          onClose={ () => this.setState({ openDialog: false }) }
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Apagar utilizador</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Tem a certeza que quer apagar este utilizador?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={ () => this.setState({ openDialog: false }) }
              className="button"
            >
              Cancelar
            </Button>
            <Button
              onClick={ () => {
                this.deleteUser(selectedUser);
                this.setState({ openDialog: false });
              } }
              className="button"
              autoFocus
            >
              Confirmar
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    users: state.user.users,
    content: state.content.content
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getUsers: (userId) => dispatch(getUsers(userId)),
    editUser: (user, userId) => dispatch(editUser(user, userId)),
    deleteUser: (user, userId) => dispatch(deleteUser(user, userId)),
    setAsAdmin: (user, userId) => dispatch(setAsAdmin(user, userId)),
    getContent: (userId) => dispatch(getContent(userId)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);
