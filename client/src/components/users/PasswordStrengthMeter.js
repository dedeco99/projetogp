import React from "react";
import zxcvbn from "zxcvbn";

import "../../css/PasswordStrengthMeter.css";

const PasswordStrengthMeter = ({ password }) => {
	const createPasswordLabel = (result) => {
    switch (result.score) {
      case 2:
        return 'Fair';
      case 3:
        return 'Good';
      case 4:
        return 'Strong';
      default:
        return 'Weak';
    }
  }

  const testedResult = zxcvbn(password);

  return (
    <div className="password-strength-meter">
			<progress
				className={`password-strength-meter-progress strength-${createPasswordLabel(testedResult)}`}
				value={ password && testedResult.score === 0 ? 1 : testedResult.score }
				max="4"
			/>
      <br />
      <label
        className="password-strength-meter-label"
      >
        {password && (
          <span>
            <strong>Password strength:</strong> {createPasswordLabel(testedResult)}
          </span>
        )}
				{password ? (
					<span>
						<br />
						{ testedResult.feedback.warning }
						<br />
						{ "Your password would take " + testedResult.crack_times_display.online_no_throttling_10_per_second + " to crack" }
					</span>
				) : (
					""
				)}
		  </label>
    </div>
  );
}

export default PasswordStrengthMeter;
