import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";

import PasswordStrengthMeter from "./PasswordStrengthMeter"

import { createUser, editUser, resetRedirect } from "../../store/actions/userActions";

class Edit extends Component {
  state = {
    name: "",
    email: "",
    password: ""
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    if(this.props.match.params.utilizador === "new"){
      this.props.createUser(this.state, this.props.auth.uid);
    }else{
      this.props.editUser(this.props.match.params.utilizador, this.state, this.props.auth.uid);
    }
  }

  render() {
    const { users, redirect, resetRedirect, error } = this.props;
    const { name, email, password } = this.state;
    const { from } = this.props.location.state || { from: { pathname: "/utilizadores" } };

    if (redirect){
      resetRedirect();
      return <Redirect to={ from } />;
    }

    if(this.props.match.params.utilizador !== "new" && users.length > 0 && email === ""){
      var editUser = this.props.users.find((user) => this.props.match.params.utilizador === user.uid);
      this.setState({
        name: editUser.displayName,
        email: editUser.email
      });
    }

    return(
      <div className="container">
        <form onSubmit={ this.handleSubmit }>
          <h5 className="grey-text text-darken-3">
            <Link id="goBack" to="/utilizadores">
              <i className="material-icons">keyboard_arrow_left</i>
            </Link>
            Utilizador
          </h5>
          <br />
          <div className="row">
            <div className="col s12">
              <TextField
                id="name"
                label="Name"
                value={ name }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <TextField
                id="email"
                type="email"
                label="Email"
                value={ email }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <TextField
                id="password"
                type="password"
                label="Password"
                value={ password }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
              <PasswordStrengthMeter
                password={password}
              />
            </div>
            <div className="col s12">
              <div className="center">
                <div className="red-text">
                  { error }
                </div>
                <br />
                <button className="btn red darken-1">Guardar</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    users: state.user.users,
    redirect: state.user.redirect,
    error: state.user.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createUser: (credentials, userId) => dispatch(createUser(credentials, userId)),
    editUser: (user, credentials, userId) => dispatch(editUser(user, credentials, userId)),
    resetRedirect: () => dispatch(resetRedirect())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
