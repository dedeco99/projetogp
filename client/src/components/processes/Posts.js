import React from "react";
import Grid from "@material-ui/core/Grid";

import Post from "./Post";

import loading from "../../img/loading.gif";

const Posts = ({ posts }) => {
	const postList = posts.length>0 ? (
		posts.map(post => {
			return (
				<Grid item xs={12} md={6} xl={4} key={ post.id }>
					<Post post={ post } />
				</Grid>
			)
		})
	) : (
		<Grid item xs={12} align="center">
			<img src={ loading } width="300px" alt="Loading..."/>
		</Grid>
	)

  return (
    <div className="Posts">
			<Grid container spacing={16}>
      	{ postList }
			</Grid>
    </div>
  );
}

export default Posts;
