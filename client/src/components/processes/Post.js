import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Tooltip from '@material-ui/core/Tooltip';

import { deleteProcess, acceptProcess } from "../../store/actions/processActions";
import { getContent } from "../../store/actions/contentActions";

import cat from "../../img/cat.png";
import dog from "../../img/dog.png";
import male from "../../img/male.png";
import female from "../../img/female.png";

class Post extends Component {
  state = {
    expanded: false,
    openDialog: false,
  }

  componentDidMount() {
    this.props.getContent(this.props.auth.uid);
  }

  deleteProcess = () => {
    const { post, auth, deleteProcess } = this.props;

    deleteProcess(post.id, auth.uid);
  }

  acceptProcess = () => {
    const { post, auth, acceptProcess } = this.props;

    acceptProcess(post.id, auth.uid);
  }

  getContent(search){
    const { content } = this.props;
    return content.length > 0 && content.find(c => c.info.page === search).info.content;
  }

  renderLinks = () => {
    const { post, auth } = this.props;

    return auth.uid ? (
      <div>
        <Link to={ "/processos/" + post.id }>
          <Tooltip title={ this.getContent("tooltipEdit") } placement="top">
            <IconButton
              className="action"
              aria-label="Edit"
            >
              <i className="material-icons">edit</i>
            </IconButton>
          </Tooltip>
        </Link>
        <Tooltip title={ this.getContent("tooltipDelete") } placement="top">
          <IconButton
            className="action"
            onClick={ () => this.setState({ openDialog: true }) }
            aria-label="Delete"
          >
            <i className="material-icons">delete</i>
          </IconButton>
        </Tooltip>
        {!post.info.accepted ?
          <Tooltip title={ this.getContent("tooltipAccept") } placement="top">
            <IconButton
              className="action"
              onClick={ this.acceptProcess }
              aria-label="Accept"
            >
              <i className="material-icons">check_circle</i>
            </IconButton>
          </Tooltip>
          : <div></div>
        }
      </div>
    ) : (
      ""
    );
  }

  render(){
    const { post, auth } = this.props;
    const { expanded, openDialog } = this.state;
    const links = this.renderLinks();

    return (
      <Card className="red darken-3">
        <CardHeader
          title={ <span style={{ color: "white" }}>{ post.info.animal.info.name }</span> }
          subheader={ <span style={{ color: "white" }}>{ post.info.type.label }</span> }
          action={
            <span className="animalAttributes">
              {
                post.info.animal.info.type.value === "cat"
                ? <img src={ cat } width="40px" alt="Cat" />
                : <img src={ dog } width="40px" alt="Dog" />
              }
              {
                post.info.animal.info.gender.value === "male"
                ? <img src={ male } width="40px" alt="Male" />
                : <img src={ female } width="40px" alt="Female" />
              }
            </span>
          }
        />
        <List>
          <Grid container spacing={8}>
            <Grid item xs={6} sm={12} lg={6}>
              <ListItem>
                <Avatar style={{ backgroundColor: "darkgrey" }}>
                  <i className="material-icons circle">date_range</i>
                </Avatar>
                <ListItemText
                  classes={{ primary: "white-text", secondary: "white-text" }}
                  primary="Nome"
                  secondary={ post.info.name }
                />
              </ListItem>
            </Grid>
            <Grid item xs={6} sm={12} lg={6}>
              <ListItem>
                <Avatar style={{ backgroundColor: "darkgrey" }}>
                  <i className="material-icons circle">phone</i>
                </Avatar>
                <ListItemText
                  classes={{ primary: "white-text", secondary: "white-text" }}
                  primary="Telefone"
                  secondary={ post.info.phone }
                />
              </ListItem>
            </Grid>
            <Grid item xs={6} sm={12} lg={6}>
              <ListItem>
                <Avatar style={{ backgroundColor: "darkgrey" }}>
                  <i className="material-icons circle">email</i>
                </Avatar>
                <ListItemText
                  classes={{ primary: "white-text", secondary: "white-text" }}
                  primary="Responsável"
                  secondary={ post.info.responsible.label }
                />
              </ListItem>
            </Grid>
            <Grid item xs={6} sm={12} lg={6}>
              <ListItem>
                <Avatar style={{ backgroundColor: "darkgrey" }}>
                  <i className="material-icons circle">email</i>
                </Avatar>
                <ListItemText
                  classes={{ primary: "white-text", secondary: "white-text" }}
                  primary="Email"
                  secondary={ post.info.email }
                />
              </ListItem>
            </Grid>
            <Grid item xs={12} xl={8}>
              <ListItem>
                <Avatar style={{ backgroundColor: "darkgrey" }}>
                  <i className="material-icons circle">description</i>
                </Avatar>
                <ListItemText
                  classes={{ primary: "white-text", secondary: "white-text" }}
                  primary="Porquê?"
                  secondary={ post.info.why }
                />
              </ListItem>
            </Grid>
          </Grid>
        </List>
        <CardActions>
          { links }
          {post.info.images.length > 0 ? (
              <IconButton
                className={ expanded ? "action expandOpen" : "action expand" }
                onClick={ () => this.setState({ expanded: !this.state.expanded }) }
                aria-expanded={ expanded }
                aria-label="Show more"
              >
                <i className="material-icons">expand_more</i>
              </IconButton>
            ) : (
              ""
            )
          }
        </CardActions>
        <Collapse in={ expanded } timeout="auto" unmountOnExit>
          <Carousel
            showArrows={ true }
            showStatus={ false }
            showThumbs={ false }
            infiniteLoop={ true }
          >
            {post.info.images.map((image, i) => (
              <div key={ i } onClick={ this.openImage }>
                <img src={ image } alt="" />
              </div>
            ))}
          </Carousel>
        </Collapse>
        <Dialog
          open={ openDialog }
          onClose={ () => this.setState({ openDialog: false }) }
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Apagar processo</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Tem a certeza que quer apagar este processo?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={ () => this.setState({ openDialog: false }) }
              className="button"
            >
              Cancelar
            </Button>
            <Button
              onClick={ () => {
                this.deleteProcess(post.id, auth.uid);
                this.setState({ openDialog: false });
              } }
              className="button"
              autoFocus
            >
              Confirmar
            </Button>
          </DialogActions>
        </Dialog>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    content: state.content.content
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    deleteProcess: (process, userId) => dispatch(deleteProcess(process, userId)),
    acceptProcess: (process, userId) => dispatch(acceptProcess(process, userId)),
    getContent: (userId) => dispatch(getContent(userId)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Post);
