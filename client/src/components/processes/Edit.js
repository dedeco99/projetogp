import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { DropzoneArea } from "material-ui-dropzone";
import axios from "axios";

import { getProcess, getSelectables, createProcess, editProcess, resetRedirect } from "../../store/actions/processActions";

class Edit extends Component {
  constructor(props){
    super(props);

    this.state = {
      animal: "",
      type: { label: "", value: "" },
      responsible: { label: "", value: "" },
      name: "",
      phone: "",
      email: "",
      why: "",
      accepted: false,
      images: [],
      files: [],

      selectables: [],
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleFileUpload = this.handleFileUpload.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFiles = this.handleFiles.bind(this);
    this.deleteImage = this.deleteImage.bind(this);
  }

  componentDidMount() {
    this.props.getSelectables(this.props.match.params.process, this.props.auth.uid, (selectables) => {
      this.setState({ selectables });
    });

    if(this.props.match.params.process !== "new"){
      this.props.getProcess(this.props.match.params.process, this.props.auth.uid, (processEdit) => {
        this.setState({
          animal: processEdit.info.animal,
          type: processEdit.info.type,
          responsible: processEdit.info.responsible,
          name: processEdit.info.name,
          phone: processEdit.info.phone,
          email: processEdit.info.email,
          why: processEdit.info.why,
          accepted: processEdit.info.accepted,
          images: processEdit.info.images,
        });
      });
    }
  }

  handleChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit(e) {
    e.preventDefault();

    const { animal, type, responsible, name, phone, email, why } = this.state;

    if(animal.id !== "" && type.value !== "" && responsible.value !== "" &&
        name !== "" && phone !== "" && email !== "" && why !== ""){
      this.handleFileUpload(() => {
        if(this.props.match.params.process === "new"){
          this.props.createProcess(this.state, this.props.auth.uid);
        }else{
          this.props.editProcess(this.props.match.params.process, this.state, this.props.auth.uid);
        }
      });
    }else{
      this.setState({ frontError: "Preencha todos os campos obrigatórios assinalados com *" });
    }
  }

  handleFileUpload(callback) {
    let filesUploaded = 0;

    if(this.state.files.length > 0){
      this.state.files.forEach((file) => {
        const formData = new FormData();
        formData.append("fileToUpload", file);
        const config = {
            headers: {
                "content-type": "multipart/form-data"
            }
        };

        axios.post("/api/processes/" + this.props.match.params.process + "/photos", formData, config)
            .then((response) => {
              if(!response.data.error){
                this.state.images.push(response.data);
              }

              filesUploaded++;

              if(filesUploaded === this.state.files.length){
                callback();
              }
            }).catch((error) => {
        });
      });
    }else{
      callback();
    }
  }

  handleFiles(files) {
      this.setState({ files });
  }

  deleteImage(e) {
    let image = e.target.id;
    let images = this.state.images.filter((value) => {
      return value !== image;
    });

    this.setState({ images });
  }

  render() {
    const { redirect, resetRedirect } = this.props;
    const { animal, type, responsible, name, phone, email, why, accepted, images, frontError, selectables } = this.state;
    const { from } = this.props.location.state || { from: { pathname: "/processos" } };

    if (redirect){
      resetRedirect();
      return <Redirect to={ from } />;
    }

    const imagePreviews = images.map((image) => {
      return (
        <div className="col s6 m3 l2 xl2" key={ image }>
          <div className="imageContainer">
            <div className="deleteImage" id={ image } onClick={ this.deleteImage }>
              <i className="material-icons" id={ image }>delete</i>
            </div>
            <img src={ image } width="100px" height="80px" alt=""/>
          </div>
        </div>
      );
    });

    return (
      <div className="container">
        <form onSubmit={ this.handleSubmit }>
          <h5 className="grey-text text-darken-3">
            <Link id="goBack" to="/processos">
              <i className="material-icons">keyboard_arrow_left</i>
            </Link>
            Processo
          </h5>
          <br/>
          <div className="row">
            <div className="col s12 m6">
              <TextField
                select
                id="animal"
                label="Animal"
                value={ animal.id ? animal.id : "" }
                onChange={ (e, value) => this.setState({ animal: { id: value.props.value } }) }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              >
                {selectables.length > 0
                    ? selectables.find(selectable => Object.keys(selectable)[0]==="animals").animals.map(animal => (
                      <MenuItem key={animal.id} value={animal.id}>
                        {animal.info.name}
                      </MenuItem>
                    ))
                    : <MenuItem key={1} value={1}>
                        Loading
                      </MenuItem>
                }
              </TextField>
            </div>
            <div className="col s12 m6">
              <TextField
                select
                id="type"
                label="Tipo de Processo"
                value={ type.value + "" }
                onChange={ (e, value) => this.setState({ type: { label: value.props.children, value: value.props.value} }) }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              >
                {selectables.length > 0
                  ? selectables.find(selectable => Object.keys(selectable)[0]==="types").types.map(type => (
                    <MenuItem key={type.value} value={type.value}>
                      {type.label}
                    </MenuItem>
                  ))
                  : <MenuItem key={1} value={1}>
                      Loading
                    </MenuItem>
                }
              </TextField>
            </div>
            <div className="col s12 m6 l4">
              <TextField
                id="name"
                label="Name"
                value={ name }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12 m6 l4">
              <TextField
                id="phone"
                label="Telefone"
                type="tel"
                value={ phone }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12 m6 l4">
              <TextField
                id="email"
                label="Email"
                type="email"
                value={ email }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <TextField
                multiline
                id="why"
                label="Porquê"
                value={ why }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12 m6">
              <TextField
                select
                id="responsible"
                label="Responsável"
                value={ responsible.value + "" }
                onChange={ (e, value) => this.setState({ responsible: { label: value.props.children, value: value.props.value } }) }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              >
                {selectables.length > 0
                  ? selectables.find(selectable => Object.keys(selectable)[0]==="responsibles").responsibles.map(responsible => (
                    <MenuItem key={responsible.value} value={responsible.value}>
                      {responsible.label}
                    </MenuItem>
                  ))
                  : <MenuItem key={1} value={1}>
                      Loading
                    </MenuItem>
              }
              </TextField>
            </div>
            <div className="col s12 m6" align="center">
              <FormControlLabel
                control={
                  <Checkbox
                    id="accepted"
                    color="default"
                    onChange={ e => this.setState({ accepted: e.target.checked }) }
                    checked={ accepted }
                  />
                }
                label="Aceite"
                style={{ padding: "20px 0px" }}
              />
            </div>
            <div className="col s12">
              <br />
              <div className="row">
                { imagePreviews }
              </div>
            </div>
            <div className="col s12">
              <div style={{ margin: "20px", color: "#777" }}>
                <DropzoneArea
                  onChange={ this.handleFiles }
                  dropzoneText={ "Arraste imagens para aqui ou clique" }
                  showAlerts={ false }
                />
              </div>
            </div>
            <div className="col s12">
              <div className="red-text center">{ frontError }</div>
              <br />
            </div>
            <div className="col s12">
              <div className="center">
                <button className="btn red darken-1">Guardar</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    redirect: state.process.redirect
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getProcess: (processId, userId, callback) => dispatch(getProcess(processId, userId, callback)),
    getSelectables: (processId, userId, callback) => dispatch(getSelectables(processId, userId, callback)),
    createProcess: (process, userId) => dispatch(createProcess(process, userId)),
    editProcess: (processId, process, userId) => dispatch(editProcess(processId, process, userId)),
    resetRedirect: () => dispatch(resetRedirect())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
