import React, { Component } from "react";
import { connect } from "react-redux";

import { getProcesses } from "../../store/actions/processActions";
import { getLogs } from "../../store/actions/userActions";

import "../../css/Dashboard.css";

class Dashboard extends Component {
  constructor(props){
    super(props);

    this.state = {
      groups: {
        type: [],
        responsible: [],
        accepted: []
      },
      logs: []
    }
  }

  calculateGroups(processes) {
    let { groups } = this.state;
    const groupables = ["type", "responsible", "accepted"];

    processes.forEach(process => {
      groupables.forEach(groupable => {
        let groupExists = false;

        if(groups[groupable].length > 0){
          groups[groupable].forEach(group => {
            let name = process.info[groupable];
            if(process.info[groupable].label){
              name = process.info[groupable].label;
              if(name === "") name = "Sem responsável";
            }else{
              if(name === true){
                name = "Aceite";
              }else{
                name = "Negado";
              }
            }

            if(group[0] === name){
              group[1]++;
              groupExists = true;
              return;
            }
          });
        }

        if(groups[groupable].length === 0 || !groupExists){
          let name = process.info[groupable];
          if(process.info[groupable].label){
            name = process.info[groupable].label;
            if(name === "") name = "Sem responsável";
          }else{
            if(name === true){
              name = "Aceite";
            }else{
              name = "Negado";
            }
          }

          groups[groupable].push([name, 1 ]);
        }
      });
    });

    this.setState({ groups });
    return groups;
  }

  calculateMonths(animals) {
    const currentDate = new Date();
    const mesesTranslate = [
      "Mês", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
      "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
    ];
    let todayMonth = currentDate.getMonth() + 1;
    let todayYear = currentDate.getFullYear();
    let meses = [];

    for (let i = 0; i < 6; i++) {
        meses.push([mesesTranslate[todayMonth], 0, 0]);
        todayMonth--;
        if (todayMonth === 0) todayMonth = 12;
    }

    animals.forEach(animal => {
      todayMonth = currentDate.getMonth() + 1;
      todayYear = currentDate.getFullYear();

      for(let i = 0; i < 6; i++){
        if(animal.info.startDate){
          if(todayMonth === new Date(animal.info.startDate).getMonth() + 1 && todayYear === new Date(animal.info.startDate).getFullYear()){
            meses[i][1]++;
          }
        }

        if(animal.info.endDate){
          if(todayMonth === new Date(animal.info.endDate).getMonth() + 1 && todayYear === new Date(animal.info.endDate).getFullYear()){
            meses[i][2]++;
          }
        }

        todayMonth--;
        if(todayMonth === 0){
          todayMonth = 12;
          todayYear--;
        }
      }
    });

    meses.reverse();
    return meses;
  }

  componentDidMount() {
    this.props.getLogs(this.props.auth.uid, (logs) => {
      logs = logs.filter(log => log.type === "process");
      console.log(logs);
      this.setState({ logs });
    });

    this.props.getProcesses(this.props.auth.uid, (animals) => {
      const groups = this.calculateGroups(animals);
      console.log(groups);
      //const meses = this.calculateMonths(animals);
      const meses = [];

      window.google.charts.load("current", {"packages":["corechart"]});
      window.google.charts.setOnLoadCallback(() => drawChart(groups, meses));

      function drawChart(groups, meses) {
        groups.responsible.unshift(["Raça", "Quantidade"]);
        var data = window.google.visualization.arrayToDataTable(groups.responsible);

        var options = {
          title: "Responsáveis",
          style: { margin: 0, padding: 0 },
          chartArea: { width: "80%", height: "80%" },
          height: 300
        };

        var chart = new window.google.visualization.PieChart(document.getElementById("piechart"));
        chart.draw(data, options);

        groups.accepted.unshift(["Aceites/Negados", "Quantidade"]);
        var data2 = window.google.visualization.arrayToDataTable(groups.accepted);

        var options2 = {
          title: "Aceites/Negados",
          style: { margin: 0, padding: 0 },
          chartArea: { width: "80%", height: "80%" },
          height: 300
        };

        var chart2 = new window.google.visualization.PieChart(document.getElementById("piechart2"));
        chart2.draw(data2, options2);
      }
    });
  }

  render() {
    const { groups, logs } = this.state;

    return (
      <div className="animalsDashboard">
        <div className="row">
          <div className="col s12 m3">
            <div className="row">
            	<div className="col s12">
                <div className="statContainer green">
                  <div className="row">
                    <div className="col s6">
                      <i className="material-icons md-36">description</i>
                    </div>
                    <div className="col s6">
                      <div className="row">
                        <div className="col s12">
                          <div className="statNumber">{ groups.type.length > 0 ? groups.type[0][1] + groups.type[1][1] : 0 }</div>
                        </div>
                        <div className="col s12">
                          <div className="unitText">Processos</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col m12">
                <div className="statContainer red darken-3">
                  <div className="row">
                    <div className="col s6">
                      <i className="material-icons md-36">home</i>
                    </div>
                    <div className="col s6">
                      <div className="row">
                        <div className="col s12">
                          <div className="statNumber">
                            { groups.type.length > 0 ? groups.type.find(type => type[0] === "Adoção")[1] : 0 }
                          </div>
                        </div>
                        <div className="col s12">
                          <div className="unitText">Adoções</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col s12">
                <div className="statContainer yellow darken-3">
                  <div className="row">
                    <div className="col s6">
                      <i className="material-icons md-36">attach_money</i>
                    </div>
                    <div className="col s6">
                      <div className="row">
                        <div className="col s12">
                          <div className="statNumber">
                            { groups.type.length > 0 ? groups.type.find(type => type[0] === "Apadrinhamento")[1] : 0 }
                          </div>
                        </div>
                        <div className="col s12">
                          <div className="unitText">Apadrinhamentos</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col s12 m9">
            <div className="row">
              <div className="col s12 l6 chart">
                <div id="piechart"></div>
              </div>
              <div className="col s12 l6 log">
                <h5>Notificações</h5>
                <ul>
                  {logs.length > 0 && logs.map((log, i) => {
              			return <li style={{ listStyleType: "square" }} key={ i }>{ log.message }</li>
              		})}
                </ul>
              </div>
              <div className="col s12 l6 chart">
                <div id="piechart2"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    processes: state.process.processes
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getProcesses: (userId, callback) => dispatch(getProcesses(userId, callback)),
    getLogs: (userId, callback) => dispatch(getLogs(userId, callback)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
