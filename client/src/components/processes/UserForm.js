import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import { DropzoneArea } from "material-ui-dropzone";
import axios from "axios";

import Posts from "../animals/Posts";

import { getAnimal } from "../../store/actions/animalActions";
import { createProcess, resetRedirect } from "../../store/actions/processActions";

class UserForm extends Component {
  constructor(props){
    super(props);

    this.state = {
      animal: { id: "" },
      type: { label: "", value: "" },
      responsible: { label: "", value: "" },
      name: "",
      phone: "",
      email: "",
      why: "",
      accepted: false,
      images: [],
      files: [],

      posts: [],
      frontError: "",
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleFileUpload = this.handleFileUpload.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFiles = this.handleFiles.bind(this);
    this.deleteImage = this.deleteImage.bind(this);
  }

  componentDidMount() {
    const { type, animal } = this.props.match.params;

    this.setState({ animal: { id: animal }, type: { value: type } });

    this.props.getAnimal(this.props.match.params.animal, this.props.auth.uid, (post) => {
      let posts = [post];
      this.setState({ posts });
    });

    let elems = document.querySelectorAll(".modal");
    window.M.Modal.init(elems);
  }

  handleChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit(e) {
    e.preventDefault();

    const { animal, type, name, phone, email, why } = this.state;

    if(animal !== "" && type !== "" && name !== "" &&
        phone !== "" && email !== "" && why !== ""){
      this.handleFileUpload(() => {
        this.props.createProcess(this.state, this.props.auth.uid);
      });
    }else{
      this.setState({ frontError: "Preencha todos os campos obrigatórios assinalados com *" });
    }
  }

  handleFileUpload(callback) {
    let filesUploaded = 0;

    if(this.state.files.length > 0){
      this.state.files.forEach((file) => {
        const formData = new FormData();
        formData.append("fileToUpload", file);
        const config = {
            headers: {
                "content-type": "multipart/form-data"
            }
        };

        axios.post("/api/processes/" + this.props.match.params.process + "/photos", formData, config)
            .then((response) => {
              if(!response.data.error){
                this.state.images.push(response.data);
              }

              filesUploaded++;

              if(filesUploaded === this.state.files.length){
                callback();
              }
            }).catch((error) => {
        });
      });
    }else{
      callback();
    }
  }

  handleFiles(files) {
      this.setState({ files });
  }

  deleteImage(e) {
    let image = e.target.id;
    let images = this.state.images.filter((value) => {
      return value !== image;
    });

    this.setState({ images });
  }

  render() {
    const { redirect, resetRedirect } = this.props;
    const { type, name, phone, email, why, posts, frontError } = this.state;
    const { from } = this.props.location.state || { from: { pathname: "/animais" } };

    if (redirect){
      resetRedirect();
      return <Redirect to={ from } />;
    }

    return (
      <div className="container">
        <form onSubmit={ this.handleSubmit }>
          <h5 className="grey-text text-darken-3">
            <Link id="goBack" to="/animais">
              <i className="material-icons">keyboard_arrow_left</i>
            </Link>
            Processo
          </h5>
          <br/>
          <div className="row">
            <div className="col s12 m6 l4">
              <TextField
                id="name"
                label="Name"
                value={ name }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12 m6 l4">
              <TextField
                id="phone"
                label="Telefone"
                type="tel"
                value={ phone }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12 m6 l4">
              <TextField
                id="email"
                label="Email"
                type="email"
                value={ email }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <TextField
                multiline
                id="why"
                label="Porquê"
                value={ why }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            { type.value === "TAtAEhFAFkkRxGYyygrR" ?
                <div className="col s12">
                  <div style={{ margin: "20px", color: "#777" }}>
                    <DropzoneArea
                      onChange={ this.handleFiles }
                      dropzoneText={ "Arraste imagens para aqui ou clique" }
                      showAlerts={ false }
                    />
                  </div>
                </div>
              :
                <div></div>
            }
            <div className="col s12">
              <div className="red-text center">{ frontError }</div>
              <br />
            </div>
            <div className="col s12">
              <div className="center">
                <button className="btn red darken-1">Guardar</button>
              </div>
            </div>
          </div>
        </form>
        <br />
        <Posts posts={ posts } />
        <div id="modalImage" className="modal">
  		    <div className="modal-content">
  		      <img id="modalImageSrc" src="" width="100%" alt="Animal fofinho" />
  		    </div>
  		  </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    redirect: state.process.redirect
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAnimal: (animalId, userId, callback) => dispatch(getAnimal(animalId, userId, callback)),
    createProcess: (process, userId) => dispatch(createProcess(process, userId)),
    resetRedirect: () => dispatch(resetRedirect())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserForm);
