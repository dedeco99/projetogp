import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Badge from "@material-ui/core/Badge";
import Tooltip from '@material-ui/core/Tooltip';

import Posts from "./Posts";

import { getProcesses } from "../../store/actions/processActions";
import { getContent } from "../../store/actions/contentActions";

class Processes extends Component {
  state = {
    open: true,
    search: "",
    orderable: "random",
    orderables: [
      { value: "random" , label: "Não Ordenado" },
      { value: "animal" , label: "Animal" },
      { value: "type" , label: "Tipo de Processo" },
      { value: "name" , label: "Nome" },
      { value: "responsible" , label: "Responsável" }
    ],
    asc: true,
    type: "all",
    types: [],
    responsible: "all",
    responsibles: [],
    ageRange: [0, 20],
    accepted: false,
    groupable: "type",
    groupables: [
      { value: "type" , label: "Tipo de Processo" },
      { value: "responsible" , label: "Responsável" }
    ],
  }

  componentDidMount() {
    this.props.getContent(this.props.auth.uid);

    this.props.getProcesses(this.props.auth.uid, (processes) => {
      let types = [], responsibles = [];

      types.push({ value: "all", label: "Todos" });
      responsibles.push({ value: "all", label: "Todos" });

      processes.forEach(process => {
        if(!types.find(type => type.value === process.info.type.value)){
          types.push({ label: process.info.type.label, value: process.info.type.value });
        }

        if(!responsibles.find(responsible => responsible.value === process.info.responsible.value)){
          let responsible = { label: process.info.responsible.label, value: process.info.responsible.value };
          if(responsible.value === "") responsible.label = "Sem responsável";

          responsibles.push(responsible);
        }
      });

      this.setState({ types, responsibles });
    });

    let elems = document.querySelectorAll(".modal");
    window.M.Modal.init(elems);
  }

  filter(e) {
    const { search, type, responsible, accepted } = this.state;
    const { processes } = this.props;

    let res = [];

    processes.forEach(process => {
      if(
        (process.info.type.value === type || type === "all") &&
        (process.info.responsible.value === responsible || responsible === "all") &&
        (process.info.accepted === accepted)
      ){
        if(
          process.info.animal.info.name.toLowerCase().includes(search.toLowerCase()) ||
          process.info.animal.info.type.label.toLowerCase().includes(search.toLowerCase()) ||
          process.info.animal.info.gender.label.toLowerCase().includes(search.toLowerCase()) ||
          process.info.animal.info.race.label.toLowerCase().includes(search.toLowerCase()) ||
          process.info.type.label.toLowerCase().includes(search.toLowerCase())
        ){
          res.push(process);
        }
      }
    });

    return this.order(res);
  }

  order(animals) {
    const { orderable, asc } = this.state;

    if(orderable === "random"){
      return animals;
    }else if(orderable === "name"){
      animals.sort((a, b) => {
        if(a.info.name.toLowerCase() < b.info.name.toLowerCase())
          return -1;
        if(a.info.name.toLowerCase() > b.info.name.toLowerCase())
          return 1;
        return 0;
      });
    }else if(orderable === "animal"){
      animals.sort((a, b) => {
        if(a.info.animal.info.name < b.info.animal.info.name)
          return -1;
        if(a.info.animal.info.name > b.info.animal.info.name)
          return 1;
        return 0;
      });
    }else{
      animals.sort((a, b) => {
        if(a.info[orderable].label < b.info[orderable].label)
          return -1;
        if(a.info[orderable].label > b.info[orderable].label)
          return 1;
        return 0;
      });
    }

    if(!asc){
      animals.reverse();
    }

    return animals;
  }

  filterGroup(group) {
    const { groupable } = this.state;

    switch(groupable){
      case "type":
        this.setState({ type: group, responsible: "all" });
        return;
      case "responsible":
        this.setState({ type: "all", responsible: group });
        return;
      default:
        return;
    }
  }

  calculateGroups() {
    const { groupable } = this.state;
    const { processes } = this.props;

    let groups = [];

    processes.forEach(process => {
      let groupExists = false;

      if(groups.length > 0){
        groups.forEach(group => {
          if(group.name.value === process.info[groupable].value){
            group.value++;
            groupExists = true;
            return;
          }
        });
      }

      if(groups.length === 0 || !groupExists){
        groups.push({ name: process.info[groupable], value: 1 });
      }
    });

    return groups;
  }

  renderGroups() {
    const groups = this.calculateGroups();

    const groupsList = groups.length>0 ? (
  		groups.map(group => {
  			return (
          <ListItem
            onClick={ () => this.filterGroup(group.name.value) }
            style={{ borderBottom: "1px solid lightgrey", color: "black", cursor: "pointer" }}
            key={ group.name.value }
          >
            { group.name.label || "Sem responsável" }
            <Badge
              color="error"
              badgeContent={ group.value }
              children={""}
              style={{ right: 25, position: "absolute" }}
            />
          </ListItem>
  			)
  		})
  	) : (
  		""
  	)
    return(
      <div>
        { groupsList }
      </div>
    );
  }

  getContent(search){
    const { content } = this.props;
    return content.length > 0 && content.find(c => c.info.page === search).info.content;
  }

  render(){
    const { processes } = this.props;
    const { open, search, orderables, orderable, asc, type, types, responsible, responsibles, accepted, groupable, groupables } = this.state;

    return (
      <div className="animals">
        <Grid container spacing={16}>
          <Grid item xs={2} sm={1}>
            <IconButton
              className={ open ? "action expand" : "action expandOpen" }
              aria-label="Open"
              onClick={() => this.setState({ open: !open })}
              style={{ margin: "18px 0px" }}
            >
              <i id="closeSidebar" className="material-icons">keyboard_arrow_left</i>
            </IconButton>
          </Grid>
          <Grid item xs={10} sm={8}>
            <TextField
              id="search"
              label="Pesquisar"
              value={ search }
              onChange={ (e) => this.setState({ search: e.target.value }) }
              InputProps={{
                classes: { notchedOutline: "input" },
                startAdornment: <InputAdornment position="start"><i className="material-icons prefix">search</i></InputAdornment>,
              }}
              margin="normal"
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item sm={3} align="center">
            <Tooltip title={ this.getContent("tooltipAdd") } placement="top">
              <Link
                to="/processos/new"
                className="addAnimal btn-floating waves-effect waves-light green darken-2"
                style={{ marginTop: "20px", marginLeft: "10px" }}
              >
                <i className="material-icons">add</i>
              </Link>
            </Tooltip>
            <Tooltip title={ this.getContent("tooltipDashboard") } placement="top">
              <Link
                to="/processos/dashboard"
                className="addAnimal btn-floating waves-effect waves-light orange darken-2"
                style={{ marginTop: "20px", marginLeft: "10px" }}
              >
                <i className="material-icons">dashboard</i>
              </Link>
            </Tooltip>
          </Grid>
        </Grid>
        <Grid container spacing={16}>
          <Grid
            item
            xs={open ? 12 : false} sm={open ? 4 : false} md={open ? 3 : false} lg={open ? 2 : false}
            style={open ? {} : { display: "none" }}
          >
            <List style={{ border: "1px solid lightgrey", borderRadius: 5, padding: 0 }}>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <h6 style={{ textAlign: "center", width: "100%", color: "black" }}>
                  <b>Filtros</b>
                </h6>
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <Grid container spacing={0}>
                  <Grid item xs={10}>
                    <TextField
                      select
                      id="orderable"
                      label="Ordenar"
                      value={ orderable }
                      onChange={ (e, value) => this.setState({ orderable: e.target.value }) }
                      InputProps={{ classes: { notchedOutline: "input" } }}
                      margin="normal"
                      variant="outlined"
                      fullWidth
                    >
                      {orderables.map(orderable => (
                        <MenuItem key={orderable.value} value={orderable.value}>
                          {orderable.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  <Grid item xs={2}>
                    <IconButton
                      className={ asc ? "action expand" : "action expandOpen" }
                      aria-label="AscDesc"
                      onClick={() => this.setState({ asc: !asc })}
                      style={{ margin: "18px 0px" }}
                    >
                      <i id="closeSidebar" className="material-icons">arrow_downward</i>
                    </IconButton>
                  </Grid>
                </Grid>
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <TextField
                  select
                  id="type"
                  label="Tipo de Processo"
                  value={ type }
                  onChange={ (e, value) => this.setState({ type: e.target.value }) }
                  InputProps={{ classes: { notchedOutline: "input" } }}
                  margin="normal"
                  variant="outlined"
                  fullWidth
                >
                  {types.map(type => (
                    <MenuItem key={type.value} value={type.value}>
                      {type.label}
                    </MenuItem>
                  ))}
                </TextField>
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <TextField
                  select
                  id="responsible"
                  label="Responsável"
                  value={ responsible }
                  onChange={ (e, value) => this.setState({ responsible: e.target.value }) }
                  InputProps={{ classes: { notchedOutline: "input" } }}
                  margin="normal"
                  variant="outlined"
                  fullWidth
                >
                  {responsibles.map(responsible => (
                    <MenuItem key={responsible.value} value={responsible.value}>
                      {responsible.label}
                    </MenuItem>
                  ))}
                </TextField>
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      id="accepted"
                      color="default"
                      onChange={ e => this.setState({ accepted: e.target.checked }) }
                      checked={ accepted }
                    />
                  }
                  label="Aceite"
                />
              </ListItem>
              <ListItem style={{ borderBottom: "1px solid lightgrey" }}>
                <TextField
                  select
                  id="groupable"
                  label="Agrupar"
                  value={ groupable }
                  onChange={ (e, value) => this.setState({ groupable: e.target.value }) }
                  InputProps={{ classes: { notchedOutline: "input" } }}
                  margin="normal"
                  variant="outlined"
                  fullWidth
                >
                  {groupables.map(groupable => (
                    <MenuItem key={groupable.value} value={groupable.value}>
                      {groupable.label}
                    </MenuItem>
                  ))}
                </TextField>
              </ListItem>
              { this.renderGroups() }
            </List>
          </Grid>
          <Grid
            item
            xs={open ? 12 : false} sm={open ? 8 : false} md={open ? 9 : false} lg={open ? 10 : false}
          >
            <Posts posts={ this.filter(processes) } />
          </Grid>
        </Grid>
        <div id="modalImage" className="modal">
  		    <div className="modal-content">
  		      <img id="modalImageSrc" src="" width="100%" alt="Animal fofinho" />
  		    </div>
  		  </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    processes: state.process.processes,
    content: state.content.content
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getProcesses: (userId, callback) => dispatch(getProcesses(userId, callback)),
    getContent: (userId) => dispatch(getContent(userId)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Processes);
