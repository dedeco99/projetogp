import React from "react";
import { connect } from "react-redux";
import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import IconButton from "@material-ui/core/IconButton";

import { closeToast } from "../../store/actions/userActions";

const Toast = ({ showToast, closeToast, toastMessage, toastType }) => {
	const getItems = () => {
		switch(toastType){
			case "warning":
				return {
					backgroundColor: "#ffa000",
					icon: "warning"
				};
			case "error":
				return {
					backgroundColor: "#d32f2f",
					icon: "error"
			};
			default:
				return {
					backgroundColor: "#43a047",
					icon: "check_circle"
			};
		}
	}

	const items = getItems();

  return (
		<Snackbar
			anchorOrigin={{
				vertical: "bottom",
				horizontal: "right",
			}}
			open={ showToast }
			onClose={ closeToast }
			autoHideDuration={ 5000 }
		>
			<SnackbarContent
				message={
					<span id="client-snackbar">
						<IconButton
							color="inherit"
						>
							<i className="material-icons">{ items.icon }</i>
						</IconButton>
						{ toastMessage }
					</span>
				}
				onClose={ closeToast }
				style={{ backgroundColor: items.backgroundColor }}
				action={[
					<IconButton
						key="close"
						aria-label="Close"
						color="inherit"
						onClick={ closeToast }
					>
						<i className="material-icons">close</i>
					</IconButton>,
				]}
			/>
		</Snackbar>
	);
}

const mapStateToProps = (state) => {
  return {
    showToast: state.user.showToast,
    toastMessage: state.user.toastMessage,
		toastType: state.user.toastType
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    closeToast: () => dispatch(closeToast())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Toast);
