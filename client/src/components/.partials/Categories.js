import React from "react";
import { Link } from "react-router-dom";

const Categories = ({ options, idField, nameField, action }) => {
  const handleClick = (e) => {
    action(e.target.id);

    var i = 0;
		var a = document.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
        a[i].classList.remove("active");
    }

    e.target.closest("a").classList.add("active");
  };

  const optionsList = options.map(option => {
    return (option.active ?
      <li className="tab col s3" onClick={ handleClick } key={ option[idField] }>
        <a id={ option[idField] } className="active">
          { option[nameField] }
        </a>
      </li>
    :
      <li className="tab col s3" onClick={ handleClick } key={ option[idField] }>
        <a id={ option[idField] }>
          { option[nameField] }
        </a>
      </li>
    )
  });

  return (
    <ul className="tabs">
      { optionsList }
    </ul>
  );
}

export default Categories;
