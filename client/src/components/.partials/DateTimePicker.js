import React, { useState } from "react";
import { MuiPickersUtilsProvider, DateTimePicker } from "material-ui-pickers";
import MomentUtils from "@date-io/moment";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core";

const materialTheme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  overrides: {
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: "#C62828",
      },
    },
    MuiPickersDay: {
      isSelected: {
				"&:hover": {
					backgroundColor: "#C62828",
				},
				"&:focus": {
					backgroundColor: "#C62828",
				},
        backgroundColor: "#C62828",
      },
      current: {
        color: "blue",
      },
    },
    MuiPickersModal: {
      dialogAction: {
        color: "#C62828",
      },
    },
		MuiPickersYear: {
			root: {
				"&:focus": {
					color: "#C62828",
					fontWeight: "bold",
					textDecoration: "underline",
				},
        "&$selected": {
  				color: "#C62828",
  				textDecoration: "underline",
  				'&:focus': {
  					color: "#C62828",
  					fontWeight: "bold",
  					textDecoration: "underline",
  				},
  			},
			},
		},
		MuiPickersMonth: {
			root: {
				"&:focus": {
					color: "#C62828",
					fontWeight: "bold",
					textDecoration: "underline",
				},
        "&$selected": {
  				color: "#C62828",
  				textDecoration: "underline",
  				'&:focus': {
  					color: "#C62828",
  					fontWeight: "bold",
  					textDecoration: "underline",
  				},
  			},
			},
		},
    MuiTabs: {
      flexContainer: {
        backgroundColor: "#9b1a1a"
      },
    },
    MuiTab: {
      root: {
				backgroundColor: "#9b1a1a",
				"&:focus": {
					backgroundColor: "#9b1a1a"
				},
			},
    },
    MuiPickersClock: {
      pin: {
        backgroundColor: "#C62828",
      },
      pointer: {
        backgroundColor: "#C62828",
      },
    },
    MuiPickersClockPointer: {
      pointer: {
        backgroundColor: "#C62828",
      },
      thumb: {
        backgroundColor: "#C62828",
        borderColor: "#C62828",
      }
    },
    MuiPickersClockNumber: {
      clockNumber: {
        "&$selected": {
          backgroundColor: "#C62828",
        },
      },
    }
  },
});

const MaterialDateTimePicker = ({ date, label, setDate }) => {
  return (
    <MuiThemeProvider theme={ materialTheme }>
			<MuiPickersUtilsProvider utils={ MomentUtils }>
				<DateTimePicker
					label={ label }
					value={ date }
					onChange={ date => setDate(date) }
					InputProps={{ classes: { notchedOutline: "input" } }}
					autoOk
          ampm={false}
					format="DD/MM/YYYY HH:mm"
					margin="normal"
					variant="outlined"
					fullWidth
				/>
			</MuiPickersUtilsProvider>
    </MuiThemeProvider>
  );
}

export default MaterialDateTimePicker;
