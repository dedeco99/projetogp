import React, { useState } from "react";
import { MuiPickersUtilsProvider, DatePicker } from "material-ui-pickers";
import MomentUtils from "@date-io/moment";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core";

const materialTheme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  overrides: {
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: "#C62828",
      },
    },
    MuiPickersDay: {
      isSelected: {
				"&:hover": {
					backgroundColor: "#C62828",
				},
				"&:focus": {
					backgroundColor: "#C62828",
				},
        backgroundColor: "#C62828",
      },
      current: {
        color: "blue",
      },
    },
    MuiPickersModal: {
      dialogAction: {
        color: "#C62828",
      },
    },
		MuiPickersYear: {
			root: {
				"&:focus": {
					color: "#C62828",
					fontWeight: "bold",
					textDecoration: "underline",
				},
        "&$selected": {
  				color: "#C62828",
  				textDecoration: "underline",
  				"&:focus": {
  					color: "#C62828",
  					fontWeight: "bold",
  					textDecoration: "underline",
  				},
  			},
			},
		},
		MuiPickersMonth: {
			root: {
				"&:focus": {
					color: "#C62828",
					fontWeight: "bold",
					textDecoration: "underline",
				},
        "&$selected": {
  				color: "#C62828",
  				textDecoration: "underline",
  				"&:focus": {
  					color: "#C62828",
  					fontWeight: "bold",
  					textDecoration: "underline",
  				},
  			},
			},
		}
  },
});

const MaterialDatePicker = ({ date, label, setDate, required }) => {
  return (
    <MuiThemeProvider theme={ materialTheme }>
			<MuiPickersUtilsProvider utils={ MomentUtils }>
				<DatePicker
					label={ label }
					value={ date }
					onChange={ date => setDate(date) }
					InputProps={{ classes: { notchedOutline: "input" } }}
					disableFuture
          autoOk
					openTo="year"
					format="DD/MM/YYYY"
					views={["year", "month", "day"]}
					margin="normal"
					variant="outlined"
					fullWidth
          required={ required }
				/>
			</MuiPickersUtilsProvider>
    </MuiThemeProvider>
  );
}

export default MaterialDatePicker;
