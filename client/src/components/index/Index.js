import React from "react";
import { connect } from "react-redux";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

import "../../css/Index.css";

const Index = ({ content }) => {
  const images = ["http://glossyinc.com/wp-content/uploads/2018/08/CatCalm_header2.jpg","https://d1vo8zfysxy97v.cloudfront.net/media/blog/why-you-should-never-fast-a-cat_fi_v6362e56b83f54d0145df06309cf2f3d6234fded3.jpg","https://www.meowlovers.com/wp-content/uploads/2019/01/what-cats-eat-2-1.jpg"];

  const contentInfo = content.find(content => content.info.page === "index");
  let contentValue = "";

  if(contentInfo) contentValue = contentInfo.info.content;

  return (
    <div className="Index">
      <div className="row">
        <div align="center" className="col m12">
          <Carousel
            showArrows={ true }
            showStatus={ false }
            showThumbs={ false }
            infiniteLoop={ true }
            autoPlay={ true }
          >
            {images.map((image, i) => (
              <div key={ i }>
                <img src={ image } alt="" />
              </div>
            ))}
          </Carousel>
        </div>
        <div dangerouslySetInnerHTML={{ __html: contentValue }} />
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    content: state.content.content
  }
}

export default connect(mapStateToProps)(Index);
