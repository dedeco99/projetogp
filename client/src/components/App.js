import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import PrivateRoute from "./auth/PrivateRoute";
import AdminRoute from "./auth/AdminRoute";

import Toast from "./.partials/Toast";
import Header from "./header/Header";
import Index from "./index/Index";
import Login from "./auth/Login";
import ForgotPassword from "./auth/ForgotPassword";
import Animals from "./animals/Animals";
import AnimalDashboard from "./animals/Dashboard";
import AnimalEdit from "./animals/Edit";
import Processes from "./processes/Processes";
import ProcessDashboard from "./processes/Dashboard";
import ProcessEdit from "./processes/Edit";
import UserForm from "./processes/UserForm";
import EventEdit from "./events/Edit";
import Events from "./events/Events";
import Users from "./users/Users";
import UserEdit from "./users/Edit";
import Content from "./content/Content";
import ContentEdit from "./content/Edit";
import Settings from "./settings/Settings";

import "../css/App.css";

const App = () => {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <div className="main">
          <Switch>
            <Route exact path="/" component={ Index } />
            <Route exact path="/login/forgotPassword" component={ ForgotPassword } />
            <Route exact path="/login" component={ Login } />
            <PrivateRoute exact path="/animais/dashboard" component={ AnimalDashboard } />
            <PrivateRoute exact path="/animais/:animal/eventos/:evento" component={ EventEdit } />
            <PrivateRoute exact path="/animais/:animal/eventos" component={ Events } />
            <PrivateRoute exact path="/animais/:animal" component={ AnimalEdit } />
            <Route exact path="/animais/:type/:animal" component={ UserForm } />
            <Route exact path="/animais" component={ Animals } />
            <PrivateRoute exact path="/processos/dashboard" component={ ProcessDashboard } />
            <PrivateRoute exact path="/processos/:process" component={ ProcessEdit } />
            <PrivateRoute exact path="/processos" component={ Processes } />
            <PrivateRoute exact path="/utilizadores/:utilizador" component={ UserEdit } />
            <AdminRoute exact path="/utilizadores" component={ Users } />
            <AdminRoute exact path="/conteudo" component={ Content } />
            <AdminRoute exact path="/conteudo/:conteudo" component={ ContentEdit } />
            <PrivateRoute exact path="/settings" component={ Settings } />
          </Switch>
          <Toast />
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
