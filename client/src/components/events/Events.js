import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Posts from "./Posts";

import { getEvents } from "../../store/actions/eventActions";

class Events extends Component {
  componentDidMount() {
    this.props.getEvents(this.props.match.params.animal, this.props.auth.uid);
  }

  render(){
    const { events, match } = this.props;

    return (
      <div style={{ marginTop: "100px" }}>
        <Link
          to={ "/animais/" + match.params.animal + "/eventos/new" }
          className="btn-floating waves-effect waves-light green darken-2"
          style={{ margin: "0px 18px", float: "right" }}
        >
          <i className="material-icons">add</i>
        </Link>
        <Posts posts={ events } />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    events: state.event.events
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getEvents: (animal, userId) => dispatch(getEvents(animal, userId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Events);
