import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

import MaterialDateTimePicker from "../.partials/DateTimePicker";

import { getEvent, getSelectables, createEvent, editEvent, resetRedirect } from "../../store/actions/eventActions";

class Edit extends Component {
  constructor(props){
    super(props);

    this.state = {
      animal: "",
      title: "",
      type: "",
      description: "",
      date: null,
      responsible: "",

      selectables: [],
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.getSelectables(this.props.match.params.evento, this.props.auth.uid, (selectables) => {
      this.setState({ selectables });
    });

    if(this.props.match.params.evento !== "new"){
      this.props.getEvent(this.props.match.params.evento, this.props.auth.uid, (eventEdit) => {
        this.setState({
          animal: eventEdit.info.animal,
          title: eventEdit.info.title,
          type: eventEdit.info.type,
          description: eventEdit.info.description,
          date: eventEdit.info.date,
          responsible: eventEdit.info.responsible
        });
      });
    }
  }

  handleChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit(e) {
    e.preventDefault();

    const {  description, responsible, type, date } = this.state;

    if(description !== "" && responsible !== "" && type !== ""){
      if(new Date(date) > new Date()){
        if(this.props.match.params.evento === "new"){
          this.props.createEvent(this.state, this.props.auth.uid);
        }else{
          this.props.editEvent(this.props.match.params.evento, this.state, this.props.auth.uid);
        }
      }else{
        this.setState({ frontError: "A data do evento tem de ser maior que hoje" });
      }
    }else{
      this.setState({ frontError: "Preencha todos os campos obrigatórios assinalados com *" });
    }
  }

  render() {
    const { redirect, resetRedirect, match } = this.props;
    const { animal, title, type, description, date, responsible, selectables, frontError } = this.state;
    const { from } = this.props.location.state || { from: { pathname: "/animais/" + animal.id + "/eventos" } };

    if (redirect){
      resetRedirect();
      return <Redirect to={ from } />;
    }

    return (
      <div className="container">
        <form onSubmit={ this.handleSubmit }>
          <h5 className="grey-text text-darken-3">
            <Link id="goBack" to={ "/animais/" + match.params.animal + "/eventos" }>
              <i className="material-icons">keyboard_arrow_left</i>
            </Link>
            Evento
          </h5>
          <br/>
          <div className="row">
            <div className="col s12 m6 l4">
              <TextField
                select
                id="animal"
                label="Animal"
                value={ animal.id ? animal.id : "" }
                onChange={ (e, value) => this.setState({ animal: { id: value.props.value } }) }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              >
                {selectables.length > 0
                    ? selectables.find(selectable => Object.keys(selectable)[0]==="animals").animals.map(animal => (
                      <MenuItem key={animal.id} value={animal.id}>
                        {animal.info.name}
                      </MenuItem>
                    ))
                    : <MenuItem key={1} value={1}>
                        Loading
                      </MenuItem>
                }
              </TextField>
            </div>
            <div className="col s12 m6 l4">
              <TextField
                id="title"
                label="Título"
                value={ title }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12 m6 l4">
              <TextField
                id="type"
                label="Tipo"
                value={ type }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <TextField
                multiline
                id="description"
                label="Descrição"
                value={ description }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12 m6">
              <MaterialDateTimePicker
                date={ date }
                label="Data/Hora do Evento"
                setDate={ date => this.setState({ date: date.format("YYYY-MM-DD HH:mm") }) }
              />
            </div>
            <div className="col s12 m6">
              <TextField
                select
                id="responsible"
                label="Responsável"
                value={ responsible.uid ? responsible.uid : "" }
                onChange={ (e, value) => this.setState({ responsible: { uid: value.props.value } }) }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              >
                {selectables.length > 0
                  ? selectables.find(selectable => Object.keys(selectable)[0]==="responsibles").responsibles.map(responsible => (
                    <MenuItem key={responsible.uid} value={responsible.uid}>
                      {responsible.displayName}
                    </MenuItem>
                  ))
                  : <MenuItem key={1} value={1}>
                      Loading
                    </MenuItem>
              }
              </TextField>
            </div>
            <div className="col s12">
              <div className="red-text center">{ frontError }</div>
              <br />
            </div>
            <div className="col s12">
              <div className="center">
                <button className="btn red darken-1">Guardar</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    redirect: state.event.redirect
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getEvent: (eventId, userId, callback) => dispatch(getEvent(eventId, userId, callback)),
    getSelectables: (eventId, userId, callback) => dispatch(getSelectables(eventId, userId, callback)),
    createEvent: (event, userId) => dispatch(createEvent(event, userId)),
    editEvent: (eventId, event, userId) => dispatch(editEvent(eventId, event, userId)),
    resetRedirect: () => dispatch(resetRedirect())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
