import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";

import { deleteEvent } from "../../store/actions/eventActions";

class Post extends Component {
  state = {
    expanded: false,
    openDialog: false,
  }

  deleteEvent = () => {
    const { post, auth, deleteEvent } = this.props;

    deleteEvent(post.id, auth.uid);
  }

  renderLinks = () => {
    const { post, auth, match } = this.props;
    const { expanded } = this.state;

    return auth.uid ? (
      <div>
        <Link to={"/animais/" + post.info.animal.id + "/eventos/" + post.id}>
          <IconButton
            className="action"
            aria-label="Edit"
          >
            <i className="material-icons">edit</i>
          </IconButton>
        </Link>
        &nbsp;&nbsp;&nbsp;
        <IconButton
          className="action"
          onClick={ () => this.setState({ openDialog: true }) }
          aria-label="Delete"
        >
          <i className="material-icons">delete</i>
        </IconButton>
        <IconButton
          className={ expanded ? "action expandOpen" : "action expand" }
          onClick={ () => this.setState({ expanded: !this.state.expanded }) }
          aria-expanded={ expanded }
          aria-label="Show more"
        >
          <i className="material-icons">expand_more</i>
        </IconButton>
      </div>
    ) : (
      ""
    );
  }

  render(){
    const { post, auth } = this.props;
    const { expanded, openDialog } = this.state;
    const links = this.renderLinks();

    return (
      <Card className="red darken-3">
        <List>
          <ListItem>
            <Avatar style={{ backgroundColor: "darkgrey" }}>
              <i className="material-icons event">event</i>
            </Avatar>
            <ListItemText
              classes={{ primary: "white-text", secondary: "white-text"}}
              primary={ post.info.title + ' - ' + post.info.type }
              secondary={ post.info.date }
            />
            { links }
          </ListItem>
          <Collapse in={ expanded } timeout="auto" unmountOnExit>
            <Grid container spacing={8}>
              <Grid item xs={12} sm={5}>
                <ListItem>
                  <Avatar style={{ backgroundColor: "darkgrey" }}>
                    <i className="material-icons circle">person</i>
                  </Avatar>
                  <ListItemText
                    classes={{ primary: "white-text", secondary: "white-text" }}
                    primary="Responsável"
                    secondary={ post.info.responsible.displayName }
                  />
                </ListItem>
              </Grid>
              <Grid item xs={12} sm={7}>
                <ListItem>
                  <Avatar style={{ backgroundColor: "darkgrey" }}>
                    <i className="material-icons circle">description</i>
                  </Avatar>
                  <ListItemText
                    classes={{ primary: "white-text", secondary: "white-text" }}
                    primary="Descrição"
                    secondary={ post.info.description }
                  />
                </ListItem>
              </Grid>
            </Grid>
          </Collapse>
        </List>
        <Dialog
          open={ openDialog }
          onClose={ () => this.setState({ openDialog: false }) }
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">Apagar evento</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Tem a certeza que quer apagar este evento?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={ () => this.setState({ openDialog: false }) }
              className="button">
              Cancelar
            </Button>
            <Button
              onClick={ () => {
                this.deleteEvent(post.id, auth.uid);
                this.setState({ openDialog: false });
              } }
              className="button"
              autoFocus>
              Confirmar
            </Button>
          </DialogActions>
        </Dialog>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    deleteEvent: (event, userId) => dispatch(deleteEvent(event, userId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Post);
