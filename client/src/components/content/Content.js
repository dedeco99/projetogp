import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { getContent, editContent } from "../../store/actions/contentActions";

class Content extends Component {
  componentDidMount() {
    this.props.getContent(this.props.auth.uid);
  }

  render(){
    const { content } = this.props;

    const contentList = content.length>0 ? (
  		content.map(content => {
  			return (
          <tr key={ content.id }>
            <td>{ content.info.page }</td>
            <td>{ content.info.content }</td>
            <td align="center">
              <Link
                to={ "/conteudo/" + content.id }
                className="btn-floating waves-effect waves-light yellow darken-2"
              >
                <i className="material-icons">edit</i>
              </Link>
            </td>
          </tr>
  			)
  		})
  	) : (
  		<tr>
  			<td colSpan="6"><div align="center">Não há conteúdo</div></td>
  		</tr>
  	)

    return (
      <div className="users container">
        <table>
          <thead>
            <tr>
              <th>Página</th>
              <th>Conteúdo</th>
              <th>Editar</th>
            </tr>
          </thead>
          <tbody>
            { contentList }
          </tbody>
        </table>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    content: state.content.content
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getContent: (userId) => dispatch(getContent(userId)),
    editContent: (content, userId) => dispatch(editContent(content, userId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Content);
