import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";

import { editContent, resetRedirect } from "../../store/actions/contentActions";

class Edit extends Component {
  state = {
    page: "",
    content: ""
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.editContent(this.props.match.params.conteudo, this.state, this.props.auth.uid);
  }

  render() {
    const { redirect, resetRedirect, error } = this.props;
    const { page, content } = this.state;
    const { from } = this.props.location.state || { from: { pathname: "/conteudo" } };
    
    if (redirect){
      resetRedirect();
      return <Redirect to={ from } />;
    }

    if(this.props.match.params.conteudo !== "new" && this.props.content.length > 0 && page === ""){
      var editContent = this.props.content.find((content) => this.props.match.params.conteudo === content.id);
      this.setState({
        page: editContent.info.page,
        content: editContent.info.content
      });
    }

    return(
      <div className="container">
        <form onSubmit={ this.handleSubmit }>
          <h5 className="grey-text text-darken-3">
            <Link id="goBack" to="/conteudo">
              <i className="material-icons">keyboard_arrow_left</i>
            </Link>
            Conteúdo
          </h5>
          <br />
          <div className="row">
            <div className="col s12">
              <TextField
                id="page"
                label="Page"
                value={ page }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <TextField
                multiline
                id="content"
                label="Content"
                value={ content }
                onChange={ this.handleChange }
                InputProps={{ classes: { notchedOutline: "input" } }}
                margin="normal"
                variant="outlined"
                fullWidth
                required
              />
            </div>
            <div className="col s12">
              <div className="center">
                <div className="red-text">
                  { error }
                </div>
                <br />
                <button className="btn red darken-1">Guardar</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    content: state.content.content,
    redirect: state.content.redirect,
    error: state.content.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    editContent: (id, content, userId) => dispatch(editContent(id, content, userId)),
    resetRedirect: () => dispatch(resetRedirect())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
