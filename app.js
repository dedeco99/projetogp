const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");

if(!process.env.DEPLOY) var secrets = require("./server/secrets.js");
const database = require("./server/database");
const users = require("./server/users");
const animals = require("./server/animals");
const processes = require("./server/processes");
const events = require("./server/events");
const content = require("./server/content");

database.initialize();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set("port",(process.env.PORT || 5000));

exports.server=app.listen(app.get("port"),function(){
  console.log("Node app is running on port",app.get("port"));
});

app.use(express.static(path.join(__dirname, "client/build")))

app.get("/healtz", (req, res) => {
  res.json({ healtz: "good" });
});

app.get("/api/users/:email/forgotPassword", users.forgotPassword);

app.get("/api/users", users.getUsers);

app.get("/api/users/:user", users.getUser);

app.post("/api/users", users.createUser);

app.put("/api/users/:user", users.editUser);

app.delete("/api/users/:user", users.deleteUser);

app.get("/api/users/:user/admin", users.setAsAdmin);

app.get("/api/logs", users.getLogs);

app.get("/api/animals", animals.getAnimals);

app.get("/api/animals/:animal", animals.getAnimal);

app.post("/api/animals", animals.createAnimal);

app.put("/api/animals/:animal", animals.editAnimal);

app.post("/api/animals/:type/photos", animals.uploadPhotos);

app.delete("/api/animals/:animal", animals.deleteAnimal);

app.get("/api/processes", processes.getProcesses);

app.get("/api/processes/:process", processes.getProcess);

app.get("/api/processes/:process/selectables", processes.getSelectables);

app.post("/api/processes", processes.createProcess);

app.put("/api/processes/:process", processes.editProcess);

app.post("/api/processes/:process/photos", processes.uploadPhotos);

app.delete("/api/processes/:process", processes.deleteProcess);

app.patch("/api/processes/:process", processes.acceptProcess);

app.get("/api/animals/:animal/events", events.getEvents);

app.get("/api/events/:event", events.getEvent);

app.get("/api/events/:event/selectables", events.getSelectables);

app.post("/api/events", events.createEvent);

app.put("/api/events/:event", events.editEvent);

app.delete("/api/events/:event", events.deleteEvent);

app.get("/api/content", content.getContent);

app.put("/api/content/:content", content.editContent);

app.get("*/", function (req, res) {
  res.sendFile(path.join(__dirname + "/client/build/index.html"))
});
